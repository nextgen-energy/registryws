
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="icpNotifications_v1Result" type="{urn:JadeWebServices/WSP_Registry2/}WS_ICPNotificationResponse"/&gt;
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "icpNotificationsV1Result",
    "message"
})
@XmlRootElement(name = "icpNotifications_v1Response")
public class IcpNotificationsV1Response {

    @XmlElement(name = "icpNotifications_v1Result", required = true, nillable = true)
    protected WSICPNotificationResponse icpNotificationsV1Result;
    @XmlElement(required = true)
    protected String message;

    /**
     * Gets the value of the icpNotificationsV1Result property.
     * 
     * @return
     *     possible object is
     *     {@link WSICPNotificationResponse }
     *     
     */
    public WSICPNotificationResponse getIcpNotificationsV1Result() {
        return icpNotificationsV1Result;
    }

    /**
     * Sets the value of the icpNotificationsV1Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSICPNotificationResponse }
     *     
     */
    public void setIcpNotificationsV1Result(WSICPNotificationResponse value) {
        this.icpNotificationsV1Result = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

}
