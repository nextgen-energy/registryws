
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Metering Installation"
 * </dict-id>
 * <dict-description>
 * "For attributes which apply to MeteringInstallation and MeteringInstallationSwitching"
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for MeteringInstallationClass complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MeteringInstallationClass"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}MeteringDetails"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allMeteringComponents" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfMeteringComponent"/&gt;
 *         &lt;element name="installationNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeteringInstallationClass", propOrder = {
    "allMeteringComponents",
    "installationNumber"
})
@XmlSeeAlso({
    MeteringInstallation.class
})
public abstract class MeteringInstallationClass
    extends MeteringDetails
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfMeteringComponent allMeteringComponents;
    protected Integer installationNumber;

    /**
     * Gets the value of the allMeteringComponents property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMeteringComponent }
     *     
     */
    public ArrayOfMeteringComponent getAllMeteringComponents() {
        return allMeteringComponents;
    }

    /**
     * Sets the value of the allMeteringComponents property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMeteringComponent }
     *     
     */
    public void setAllMeteringComponents(ArrayOfMeteringComponent value) {
        this.allMeteringComponents = value;
    }

    /**
     * Gets the value of the installationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInstallationNumber() {
        return installationNumber;
    }

    /**
     * Sets the value of the installationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInstallationNumber(Integer value) {
        this.installationNumber = value;
    }

}
