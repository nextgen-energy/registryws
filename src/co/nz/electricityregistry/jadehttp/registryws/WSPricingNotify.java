
package co.nz.electricityregistry.jadehttp.registryws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_PricingNotify complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_PricingNotify"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_EventNotification"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="chargeableCapacity" type="{urn:JadeWebServices/WSP_Registry2/}decimal_9_2" minOccurs="0"/&gt;
 *         &lt;element name="distributorInstallationDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="distributorLossCategoryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="distributorPriceCategoryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_PricingNotify", propOrder = {
    "chargeableCapacity",
    "distributorInstallationDetails",
    "distributorLossCategoryCode",
    "distributorPriceCategoryCode"
})
public class WSPricingNotify
    extends WSEventNotification
{

    protected BigDecimal chargeableCapacity;
    protected String distributorInstallationDetails;
    protected String distributorLossCategoryCode;
    protected String distributorPriceCategoryCode;

    /**
     * Gets the value of the chargeableCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChargeableCapacity() {
        return chargeableCapacity;
    }

    /**
     * Sets the value of the chargeableCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChargeableCapacity(BigDecimal value) {
        this.chargeableCapacity = value;
    }

    /**
     * Gets the value of the distributorInstallationDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributorInstallationDetails() {
        return distributorInstallationDetails;
    }

    /**
     * Sets the value of the distributorInstallationDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributorInstallationDetails(String value) {
        this.distributorInstallationDetails = value;
    }

    /**
     * Gets the value of the distributorLossCategoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributorLossCategoryCode() {
        return distributorLossCategoryCode;
    }

    /**
     * Sets the value of the distributorLossCategoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributorLossCategoryCode(String value) {
        this.distributorLossCategoryCode = value;
    }

    /**
     * Gets the value of the distributorPriceCategoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributorPriceCategoryCode() {
        return distributorPriceCategoryCode;
    }

    /**
     * Sets the value of the distributorPriceCategoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributorPriceCategoryCode(String value) {
        this.distributorPriceCategoryCode = value;
    }

}
