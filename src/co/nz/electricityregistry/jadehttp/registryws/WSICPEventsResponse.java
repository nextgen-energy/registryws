
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_ICPEventsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_ICPEventsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_Response"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allEvents" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfWS_ICPEvent"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_ICPEventsResponse", propOrder = {
    "allEvents"
})
public class WSICPEventsResponse
    extends WSResponse
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfWSICPEvent allEvents;

    /**
     * Gets the value of the allEvents property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSICPEvent }
     *     
     */
    public ArrayOfWSICPEvent getAllEvents() {
        return allEvents;
    }

    /**
     * Sets the value of the allEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSICPEvent }
     *     
     */
    public void setAllEvents(ArrayOfWSICPEvent value) {
        this.allEvents = value;
    }

}
