
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WS_ICPEvent_v2 complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_ICPEvent_v2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_WebServiceTransients"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="auditNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="creationTimeStamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="eventDataSummary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="eventType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="isReplaced" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isReversed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="reverseReplaceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="userIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_ICPEvent_v2", propOrder = {
    "auditNumber",
    "creationTimeStamp",
    "eventDataSummary",
    "eventDate",
    "eventType",
    "isReplaced",
    "isReversed",
    "reverseReplaceDate",
    "userIdentifier"
})
@XmlSeeAlso({
    WSICPEventMeterV2 .class
})
public class WSICPEventV2
    extends WSWebServiceTransients
{

    protected String auditNumber;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationTimeStamp;
    protected String eventDataSummary;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar eventDate;
    protected String eventType;
    protected Boolean isReplaced;
    protected Boolean isReversed;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar reverseReplaceDate;
    protected String userIdentifier;

    /**
     * Gets the value of the auditNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuditNumber() {
        return auditNumber;
    }

    /**
     * Sets the value of the auditNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuditNumber(String value) {
        this.auditNumber = value;
    }

    /**
     * Gets the value of the creationTimeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationTimeStamp() {
        return creationTimeStamp;
    }

    /**
     * Sets the value of the creationTimeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationTimeStamp(XMLGregorianCalendar value) {
        this.creationTimeStamp = value;
    }

    /**
     * Gets the value of the eventDataSummary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventDataSummary() {
        return eventDataSummary;
    }

    /**
     * Sets the value of the eventDataSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventDataSummary(String value) {
        this.eventDataSummary = value;
    }

    /**
     * Gets the value of the eventDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventDate() {
        return eventDate;
    }

    /**
     * Sets the value of the eventDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventDate(XMLGregorianCalendar value) {
        this.eventDate = value;
    }

    /**
     * Gets the value of the eventType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventType(String value) {
        this.eventType = value;
    }

    /**
     * Gets the value of the isReplaced property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReplaced() {
        return isReplaced;
    }

    /**
     * Sets the value of the isReplaced property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReplaced(Boolean value) {
        this.isReplaced = value;
    }

    /**
     * Gets the value of the isReversed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReversed() {
        return isReversed;
    }

    /**
     * Sets the value of the isReversed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReversed(Boolean value) {
        this.isReversed = value;
    }

    /**
     * Gets the value of the reverseReplaceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReverseReplaceDate() {
        return reverseReplaceDate;
    }

    /**
     * Sets the value of the reverseReplaceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReverseReplaceDate(XMLGregorianCalendar value) {
        this.reverseReplaceDate = value;
    }

    /**
     * Gets the value of the userIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserIdentifier() {
        return userIdentifier;
    }

    /**
     * Sets the value of the userIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserIdentifier(String value) {
        this.userIdentifier = value;
    }

}
