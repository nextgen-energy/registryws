
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WS_MeterInstallationNotify complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_MeterInstallationNotify"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_EventNotificationParent"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allComponents" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfWS_MeterComponentNotify"/&gt;
 *         &lt;element name="athParticipantIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="certificationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="certificationVariations" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="certificationVariationsExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="controlDeviceCertificationFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="highestMeteringCategory" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="leasePriceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="maximumInterogationCycle" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="meteringInstallationCertificationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="meteringInstallationCertificationExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="meteringInstallationCertificationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="meteringInstallationLocationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="meteringInstallationNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="meteringInstallationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numberOfComponents" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_MeterInstallationNotify", propOrder = {
    "allComponents",
    "athParticipantIdentifier",
    "certificationNumber",
    "certificationVariations",
    "certificationVariationsExpiryDate",
    "controlDeviceCertificationFlag",
    "highestMeteringCategory",
    "leasePriceCode",
    "maximumInterogationCycle",
    "meteringInstallationCertificationDate",
    "meteringInstallationCertificationExpiryDate",
    "meteringInstallationCertificationType",
    "meteringInstallationLocationCode",
    "meteringInstallationNumber",
    "meteringInstallationType",
    "numberOfComponents"
})
public class WSMeterInstallationNotify
    extends WSEventNotificationParent
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfWSMeterComponentNotify allComponents;
    protected String athParticipantIdentifier;
    protected String certificationNumber;
    protected String certificationVariations;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar certificationVariationsExpiryDate;
    protected Boolean controlDeviceCertificationFlag;
    protected Integer highestMeteringCategory;
    protected String leasePriceCode;
    protected Integer maximumInterogationCycle;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar meteringInstallationCertificationDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar meteringInstallationCertificationExpiryDate;
    protected String meteringInstallationCertificationType;
    protected String meteringInstallationLocationCode;
    protected Integer meteringInstallationNumber;
    protected String meteringInstallationType;
    protected Integer numberOfComponents;

    /**
     * Gets the value of the allComponents property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSMeterComponentNotify }
     *     
     */
    public ArrayOfWSMeterComponentNotify getAllComponents() {
        return allComponents;
    }

    /**
     * Sets the value of the allComponents property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSMeterComponentNotify }
     *     
     */
    public void setAllComponents(ArrayOfWSMeterComponentNotify value) {
        this.allComponents = value;
    }

    /**
     * Gets the value of the athParticipantIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAthParticipantIdentifier() {
        return athParticipantIdentifier;
    }

    /**
     * Sets the value of the athParticipantIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAthParticipantIdentifier(String value) {
        this.athParticipantIdentifier = value;
    }

    /**
     * Gets the value of the certificationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificationNumber() {
        return certificationNumber;
    }

    /**
     * Sets the value of the certificationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificationNumber(String value) {
        this.certificationNumber = value;
    }

    /**
     * Gets the value of the certificationVariations property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificationVariations() {
        return certificationVariations;
    }

    /**
     * Sets the value of the certificationVariations property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificationVariations(String value) {
        this.certificationVariations = value;
    }

    /**
     * Gets the value of the certificationVariationsExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCertificationVariationsExpiryDate() {
        return certificationVariationsExpiryDate;
    }

    /**
     * Sets the value of the certificationVariationsExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCertificationVariationsExpiryDate(XMLGregorianCalendar value) {
        this.certificationVariationsExpiryDate = value;
    }

    /**
     * Gets the value of the controlDeviceCertificationFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isControlDeviceCertificationFlag() {
        return controlDeviceCertificationFlag;
    }

    /**
     * Sets the value of the controlDeviceCertificationFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setControlDeviceCertificationFlag(Boolean value) {
        this.controlDeviceCertificationFlag = value;
    }

    /**
     * Gets the value of the highestMeteringCategory property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHighestMeteringCategory() {
        return highestMeteringCategory;
    }

    /**
     * Sets the value of the highestMeteringCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHighestMeteringCategory(Integer value) {
        this.highestMeteringCategory = value;
    }

    /**
     * Gets the value of the leasePriceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeasePriceCode() {
        return leasePriceCode;
    }

    /**
     * Sets the value of the leasePriceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeasePriceCode(String value) {
        this.leasePriceCode = value;
    }

    /**
     * Gets the value of the maximumInterogationCycle property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximumInterogationCycle() {
        return maximumInterogationCycle;
    }

    /**
     * Sets the value of the maximumInterogationCycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximumInterogationCycle(Integer value) {
        this.maximumInterogationCycle = value;
    }

    /**
     * Gets the value of the meteringInstallationCertificationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeteringInstallationCertificationDate() {
        return meteringInstallationCertificationDate;
    }

    /**
     * Sets the value of the meteringInstallationCertificationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeteringInstallationCertificationDate(XMLGregorianCalendar value) {
        this.meteringInstallationCertificationDate = value;
    }

    /**
     * Gets the value of the meteringInstallationCertificationExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeteringInstallationCertificationExpiryDate() {
        return meteringInstallationCertificationExpiryDate;
    }

    /**
     * Sets the value of the meteringInstallationCertificationExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeteringInstallationCertificationExpiryDate(XMLGregorianCalendar value) {
        this.meteringInstallationCertificationExpiryDate = value;
    }

    /**
     * Gets the value of the meteringInstallationCertificationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeteringInstallationCertificationType() {
        return meteringInstallationCertificationType;
    }

    /**
     * Sets the value of the meteringInstallationCertificationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeteringInstallationCertificationType(String value) {
        this.meteringInstallationCertificationType = value;
    }

    /**
     * Gets the value of the meteringInstallationLocationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeteringInstallationLocationCode() {
        return meteringInstallationLocationCode;
    }

    /**
     * Sets the value of the meteringInstallationLocationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeteringInstallationLocationCode(String value) {
        this.meteringInstallationLocationCode = value;
    }

    /**
     * Gets the value of the meteringInstallationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMeteringInstallationNumber() {
        return meteringInstallationNumber;
    }

    /**
     * Sets the value of the meteringInstallationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMeteringInstallationNumber(Integer value) {
        this.meteringInstallationNumber = value;
    }

    /**
     * Gets the value of the meteringInstallationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeteringInstallationType() {
        return meteringInstallationType;
    }

    /**
     * Sets the value of the meteringInstallationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeteringInstallationType(String value) {
        this.meteringInstallationType = value;
    }

    /**
     * Gets the value of the numberOfComponents property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfComponents() {
        return numberOfComponents;
    }

    /**
     * Sets the value of the numberOfComponents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfComponents(Integer value) {
        this.numberOfComponents = value;
    }

}
