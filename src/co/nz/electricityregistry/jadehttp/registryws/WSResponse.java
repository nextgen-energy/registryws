
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_Response complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_Response"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_WebServiceTransients"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allErrors" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfWS_Error"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_Response", propOrder = {
    "allErrors"
})
@XmlSeeAlso({
    WSEaVerificationWebServicetResponse.class,
    WSICPDetailsResponse.class,
    WSICPEventsResponse.class,
    WSICPEventsResponseV2 .class,
    WSICPSearchResponse.class,
    WSICPNotificationResponse.class
})
public abstract class WSResponse
    extends WSWebServiceTransients
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfWSError allErrors;

    /**
     * Gets the value of the allErrors property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSError }
     *     
     */
    public ArrayOfWSError getAllErrors() {
        return allErrors;
    }

    /**
     * Sets the value of the allErrors property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSError }
     *     
     */
    public void setAllErrors(ArrayOfWSError value) {
        this.allErrors = value;
    }

}
