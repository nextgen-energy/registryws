
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Metering Channel"
 * </dict-id>
 * <dict-description>
 * "For attributes which apply to MeteringChannel and MeteringChannelSwitching"
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for MeteringChannelClass complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MeteringChannelClass"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}MeteringDetails"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="channelNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeteringChannelClass", propOrder = {
    "channelNumber"
})
@XmlSeeAlso({
    MeteringChannel.class
})
public abstract class MeteringChannelClass
    extends MeteringDetails
{

    protected Integer channelNumber;

    /**
     * Gets the value of the channelNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChannelNumber() {
        return channelNumber;
    }

    /**
     * Sets the value of the channelNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChannelNumber(Integer value) {
        this.channelNumber = value;
    }

}
