
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="userName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="icpId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="includeReversed" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="includeSwitch" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="includeTrader" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="includeNetwork" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="includePricing" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="includeStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="includeAddress" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="includeMeter" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userName",
    "password",
    "icpId",
    "includeReversed",
    "includeSwitch",
    "includeTrader",
    "includeNetwork",
    "includePricing",
    "includeStatus",
    "includeAddress",
    "includeMeter"
})
@XmlRootElement(name = "icpEvents_v2")
public class IcpEventsV2 {

    @XmlElement(required = true)
    protected String userName;
    @XmlElement(required = true)
    protected String password;
    @XmlElement(required = true)
    protected String icpId;
    protected boolean includeReversed;
    protected boolean includeSwitch;
    protected boolean includeTrader;
    protected boolean includeNetwork;
    protected boolean includePricing;
    protected boolean includeStatus;
    protected boolean includeAddress;
    protected boolean includeMeter;

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the icpId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcpId() {
        return icpId;
    }

    /**
     * Sets the value of the icpId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcpId(String value) {
        this.icpId = value;
    }

    /**
     * Gets the value of the includeReversed property.
     * 
     */
    public boolean isIncludeReversed() {
        return includeReversed;
    }

    /**
     * Sets the value of the includeReversed property.
     * 
     */
    public void setIncludeReversed(boolean value) {
        this.includeReversed = value;
    }

    /**
     * Gets the value of the includeSwitch property.
     * 
     */
    public boolean isIncludeSwitch() {
        return includeSwitch;
    }

    /**
     * Sets the value of the includeSwitch property.
     * 
     */
    public void setIncludeSwitch(boolean value) {
        this.includeSwitch = value;
    }

    /**
     * Gets the value of the includeTrader property.
     * 
     */
    public boolean isIncludeTrader() {
        return includeTrader;
    }

    /**
     * Sets the value of the includeTrader property.
     * 
     */
    public void setIncludeTrader(boolean value) {
        this.includeTrader = value;
    }

    /**
     * Gets the value of the includeNetwork property.
     * 
     */
    public boolean isIncludeNetwork() {
        return includeNetwork;
    }

    /**
     * Sets the value of the includeNetwork property.
     * 
     */
    public void setIncludeNetwork(boolean value) {
        this.includeNetwork = value;
    }

    /**
     * Gets the value of the includePricing property.
     * 
     */
    public boolean isIncludePricing() {
        return includePricing;
    }

    /**
     * Sets the value of the includePricing property.
     * 
     */
    public void setIncludePricing(boolean value) {
        this.includePricing = value;
    }

    /**
     * Gets the value of the includeStatus property.
     * 
     */
    public boolean isIncludeStatus() {
        return includeStatus;
    }

    /**
     * Sets the value of the includeStatus property.
     * 
     */
    public void setIncludeStatus(boolean value) {
        this.includeStatus = value;
    }

    /**
     * Gets the value of the includeAddress property.
     * 
     */
    public boolean isIncludeAddress() {
        return includeAddress;
    }

    /**
     * Sets the value of the includeAddress property.
     * 
     */
    public void setIncludeAddress(boolean value) {
        this.includeAddress = value;
    }

    /**
     * Gets the value of the includeMeter property.
     * 
     */
    public boolean isIncludeMeter() {
        return includeMeter;
    }

    /**
     * Sets the value of the includeMeter property.
     * 
     */
    public void setIncludeMeter(boolean value) {
        this.includeMeter = value;
    }

}
