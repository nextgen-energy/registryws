
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_ICPSearchResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_ICPSearchResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_Response"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allResults" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfWS_ICPSearchResult"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_ICPSearchResponse", propOrder = {
    "allResults"
})
public class WSICPSearchResponse
    extends WSResponse
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfWSICPSearchResult allResults;

    /**
     * Gets the value of the allResults property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSICPSearchResult }
     *     
     */
    public ArrayOfWSICPSearchResult getAllResults() {
        return allResults;
    }

    /**
     * Sets the value of the allResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSICPSearchResult }
     *     
     */
    public void setAllResults(ArrayOfWSICPSearchResult value) {
        this.allResults = value;
    }

}
