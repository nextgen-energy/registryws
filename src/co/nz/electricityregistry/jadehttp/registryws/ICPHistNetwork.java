
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Network"
 * </dict-id>
 * <dict-description>
 * "This event type groups together the attributes relating to network connection details and the network owner (Distributor):
 * * Event Date;
 * * Network Participant Identifier;
 * * POC;
 * * Reconciliation Type;
 * * Dedicated NSP;
 * * Installation Type;
 * * Proposed Trader;
 * * Unmetered Load Details - Distributor;
 * * Shared ICP List;
 * * Generation Capacity;
 * * Fuel Type;
 * * Initial  Energisation Date;
 * * Direct Billed Status;
 * * Direct Billed Details; and
 * * User Reference
 * The NSP Identifier is a composite code made up of the Network Participant Identifier and the POC.  All valid NSP identifier combinations are maintained by the Reconciliation Manager in the registry.  Distributors can only use the NSP identifiers where they are recorded as being the owner in the NSP mapping table.
 * * The NSP Identifier is assigned in the following format: bbbqqqz nnnn. Where bbbqqqz is the POC and nnnn is the Network Participant Identifier."
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for ICPHistNetwork complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ICPHistNetwork"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}ICPHistory"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allSharedICPsList" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfICP"/&gt;
 *         &lt;element name="bus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dedicatedNSP" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="directBilledDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="generationCapacityKW" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="initialEnergisationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="installationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="myNetwork" type="{urn:JadeWebServices/WSP_Registry2/}Participant"/&gt;
 *         &lt;element name="networkIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="proposedRetailer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="unmeteredLoadDistributor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vDirectBilledStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vGenerationFuelType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vInstallationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vMyNSPMapping" type="{urn:JadeWebServices/WSP_Registry2/}NSPMapping"/&gt;
 *         &lt;element name="vProposedTrader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vReconciliationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ICPHistNetwork", propOrder = {
    "allSharedICPsList",
    "bus",
    "dedicatedNSP",
    "directBilledDetails",
    "generationCapacityKW",
    "initialEnergisationDate",
    "installationType",
    "myNetwork",
    "networkIdentifier",
    "proposedRetailer",
    "unmeteredLoadDistributor",
    "vDirectBilledStatus",
    "vGenerationFuelType",
    "vInstallationType",
    "vMyNSPMapping",
    "vProposedTrader",
    "vReconciliationType"
})
public class ICPHistNetwork
    extends ICPHistory
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfICP allSharedICPsList;
    protected String bus;
    protected Boolean dedicatedNSP;
    protected String directBilledDetails;
    protected Double generationCapacityKW;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar initialEnergisationDate;
    protected String installationType;
    @XmlElement(required = true, nillable = true)
    protected Participant myNetwork;
    protected String networkIdentifier;
    protected String proposedRetailer;
    protected String unmeteredLoadDistributor;
    protected String vDirectBilledStatus;
    protected String vGenerationFuelType;
    protected String vInstallationType;
    @XmlElement(required = true, nillable = true)
    protected NSPMapping vMyNSPMapping;
    protected String vProposedTrader;
    protected String vReconciliationType;

    /**
     * Gets the value of the allSharedICPsList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfICP }
     *     
     */
    public ArrayOfICP getAllSharedICPsList() {
        return allSharedICPsList;
    }

    /**
     * Sets the value of the allSharedICPsList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfICP }
     *     
     */
    public void setAllSharedICPsList(ArrayOfICP value) {
        this.allSharedICPsList = value;
    }

    /**
     * Gets the value of the bus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBus() {
        return bus;
    }

    /**
     * Sets the value of the bus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBus(String value) {
        this.bus = value;
    }

    /**
     * Gets the value of the dedicatedNSP property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDedicatedNSP() {
        return dedicatedNSP;
    }

    /**
     * Sets the value of the dedicatedNSP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDedicatedNSP(Boolean value) {
        this.dedicatedNSP = value;
    }

    /**
     * Gets the value of the directBilledDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectBilledDetails() {
        return directBilledDetails;
    }

    /**
     * Sets the value of the directBilledDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectBilledDetails(String value) {
        this.directBilledDetails = value;
    }

    /**
     * Gets the value of the generationCapacityKW property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getGenerationCapacityKW() {
        return generationCapacityKW;
    }

    /**
     * Sets the value of the generationCapacityKW property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setGenerationCapacityKW(Double value) {
        this.generationCapacityKW = value;
    }

    /**
     * Gets the value of the initialEnergisationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInitialEnergisationDate() {
        return initialEnergisationDate;
    }

    /**
     * Sets the value of the initialEnergisationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInitialEnergisationDate(XMLGregorianCalendar value) {
        this.initialEnergisationDate = value;
    }

    /**
     * Gets the value of the installationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstallationType() {
        return installationType;
    }

    /**
     * Sets the value of the installationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstallationType(String value) {
        this.installationType = value;
    }

    /**
     * Gets the value of the myNetwork property.
     * 
     * @return
     *     possible object is
     *     {@link Participant }
     *     
     */
    public Participant getMyNetwork() {
        return myNetwork;
    }

    /**
     * Sets the value of the myNetwork property.
     * 
     * @param value
     *     allowed object is
     *     {@link Participant }
     *     
     */
    public void setMyNetwork(Participant value) {
        this.myNetwork = value;
    }

    /**
     * Gets the value of the networkIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkIdentifier() {
        return networkIdentifier;
    }

    /**
     * Sets the value of the networkIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkIdentifier(String value) {
        this.networkIdentifier = value;
    }

    /**
     * Gets the value of the proposedRetailer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposedRetailer() {
        return proposedRetailer;
    }

    /**
     * Sets the value of the proposedRetailer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposedRetailer(String value) {
        this.proposedRetailer = value;
    }

    /**
     * Gets the value of the unmeteredLoadDistributor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnmeteredLoadDistributor() {
        return unmeteredLoadDistributor;
    }

    /**
     * Sets the value of the unmeteredLoadDistributor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnmeteredLoadDistributor(String value) {
        this.unmeteredLoadDistributor = value;
    }

    /**
     * Gets the value of the vDirectBilledStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVDirectBilledStatus() {
        return vDirectBilledStatus;
    }

    /**
     * Sets the value of the vDirectBilledStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVDirectBilledStatus(String value) {
        this.vDirectBilledStatus = value;
    }

    /**
     * Gets the value of the vGenerationFuelType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVGenerationFuelType() {
        return vGenerationFuelType;
    }

    /**
     * Sets the value of the vGenerationFuelType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVGenerationFuelType(String value) {
        this.vGenerationFuelType = value;
    }

    /**
     * Gets the value of the vInstallationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVInstallationType() {
        return vInstallationType;
    }

    /**
     * Sets the value of the vInstallationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVInstallationType(String value) {
        this.vInstallationType = value;
    }

    /**
     * Gets the value of the vMyNSPMapping property.
     * 
     * @return
     *     possible object is
     *     {@link NSPMapping }
     *     
     */
    public NSPMapping getVMyNSPMapping() {
        return vMyNSPMapping;
    }

    /**
     * Sets the value of the vMyNSPMapping property.
     * 
     * @param value
     *     allowed object is
     *     {@link NSPMapping }
     *     
     */
    public void setVMyNSPMapping(NSPMapping value) {
        this.vMyNSPMapping = value;
    }

    /**
     * Gets the value of the vProposedTrader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVProposedTrader() {
        return vProposedTrader;
    }

    /**
     * Sets the value of the vProposedTrader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVProposedTrader(String value) {
        this.vProposedTrader = value;
    }

    /**
     * Gets the value of the vReconciliationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVReconciliationType() {
        return vReconciliationType;
    }

    /**
     * Sets the value of the vReconciliationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVReconciliationType(String value) {
        this.vReconciliationType = value;
    }

}
