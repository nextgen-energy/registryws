
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Trader"
 * </dict-id>
 * <dict-description>
 * "This event type groups together the attributes used for reconciliation and for identifying the current Trader:
 * * Event Date;
 * * Trader Participant Identifier;
 * * Profile(s);
 * * Proposed MEP
 * * UNM Flag
 * * Daily Unmetered kWh
 * * Unmetered Load Details - Trader
 * * Submission Type HHR;
 * * Submission Type NHH;
 * * ANZSIC; and
 * * User Reference."
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for ICPHistRecon complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ICPHistRecon"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}ICPHistory"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allProfiles" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfRetailerProfile"/&gt;
 *         &lt;element name="dailyUnmeteredkWh" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="myRetailer" type="{urn:JadeWebServices/WSP_Registry2/}Participant"/&gt;
 *         &lt;element name="submissionTypeHHR" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="submissionTypeNHH" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="traderIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="unmFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="unmeteredLoadTrader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vANZSICcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vMEPAcceptedNomination" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="vProfileCodeList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vProposedMEPidentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vTraderCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ICPHistRecon", propOrder = {
    "allProfiles",
    "dailyUnmeteredkWh",
    "myRetailer",
    "submissionTypeHHR",
    "submissionTypeNHH",
    "traderIdentifier",
    "unmFlag",
    "unmeteredLoadTrader",
    "vanzsiCcode",
    "vmepAcceptedNomination",
    "vProfileCodeList",
    "vProposedMEPidentifier",
    "vTraderCode"
})
public class ICPHistRecon
    extends ICPHistory
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfRetailerProfile allProfiles;
    protected String dailyUnmeteredkWh;
    @XmlElement(required = true, nillable = true)
    protected Participant myRetailer;
    protected Boolean submissionTypeHHR;
    protected Boolean submissionTypeNHH;
    protected String traderIdentifier;
    protected Boolean unmFlag;
    protected String unmeteredLoadTrader;
    @XmlElement(name = "vANZSICcode")
    protected String vanzsiCcode;
    @XmlElement(name = "vMEPAcceptedNomination")
    protected Boolean vmepAcceptedNomination;
    protected String vProfileCodeList;
    protected String vProposedMEPidentifier;
    protected String vTraderCode;

    /**
     * Gets the value of the allProfiles property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRetailerProfile }
     *     
     */
    public ArrayOfRetailerProfile getAllProfiles() {
        return allProfiles;
    }

    /**
     * Sets the value of the allProfiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRetailerProfile }
     *     
     */
    public void setAllProfiles(ArrayOfRetailerProfile value) {
        this.allProfiles = value;
    }

    /**
     * Gets the value of the dailyUnmeteredkWh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDailyUnmeteredkWh() {
        return dailyUnmeteredkWh;
    }

    /**
     * Sets the value of the dailyUnmeteredkWh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDailyUnmeteredkWh(String value) {
        this.dailyUnmeteredkWh = value;
    }

    /**
     * Gets the value of the myRetailer property.
     * 
     * @return
     *     possible object is
     *     {@link Participant }
     *     
     */
    public Participant getMyRetailer() {
        return myRetailer;
    }

    /**
     * Sets the value of the myRetailer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Participant }
     *     
     */
    public void setMyRetailer(Participant value) {
        this.myRetailer = value;
    }

    /**
     * Gets the value of the submissionTypeHHR property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSubmissionTypeHHR() {
        return submissionTypeHHR;
    }

    /**
     * Sets the value of the submissionTypeHHR property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubmissionTypeHHR(Boolean value) {
        this.submissionTypeHHR = value;
    }

    /**
     * Gets the value of the submissionTypeNHH property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSubmissionTypeNHH() {
        return submissionTypeNHH;
    }

    /**
     * Sets the value of the submissionTypeNHH property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubmissionTypeNHH(Boolean value) {
        this.submissionTypeNHH = value;
    }

    /**
     * Gets the value of the traderIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTraderIdentifier() {
        return traderIdentifier;
    }

    /**
     * Sets the value of the traderIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTraderIdentifier(String value) {
        this.traderIdentifier = value;
    }

    /**
     * Gets the value of the unmFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnmFlag() {
        return unmFlag;
    }

    /**
     * Sets the value of the unmFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnmFlag(Boolean value) {
        this.unmFlag = value;
    }

    /**
     * Gets the value of the unmeteredLoadTrader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnmeteredLoadTrader() {
        return unmeteredLoadTrader;
    }

    /**
     * Sets the value of the unmeteredLoadTrader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnmeteredLoadTrader(String value) {
        this.unmeteredLoadTrader = value;
    }

    /**
     * Gets the value of the vanzsiCcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVANZSICcode() {
        return vanzsiCcode;
    }

    /**
     * Sets the value of the vanzsiCcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVANZSICcode(String value) {
        this.vanzsiCcode = value;
    }

    /**
     * Gets the value of the vmepAcceptedNomination property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVMEPAcceptedNomination() {
        return vmepAcceptedNomination;
    }

    /**
     * Sets the value of the vmepAcceptedNomination property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVMEPAcceptedNomination(Boolean value) {
        this.vmepAcceptedNomination = value;
    }

    /**
     * Gets the value of the vProfileCodeList property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVProfileCodeList() {
        return vProfileCodeList;
    }

    /**
     * Sets the value of the vProfileCodeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVProfileCodeList(String value) {
        this.vProfileCodeList = value;
    }

    /**
     * Gets the value of the vProposedMEPidentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVProposedMEPidentifier() {
        return vProposedMEPidentifier;
    }

    /**
     * Sets the value of the vProposedMEPidentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVProposedMEPidentifier(String value) {
        this.vProposedMEPidentifier = value;
    }

    /**
     * Gets the value of the vTraderCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVTraderCode() {
        return vTraderCode;
    }

    /**
     * Sets the value of the vTraderCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVTraderCode(String value) {
        this.vTraderCode = value;
    }

}
