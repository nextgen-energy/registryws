
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_ICPDetailsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_ICPDetailsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_Response"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="myAddressHistory" type="{urn:JadeWebServices/WSP_Registry2/}ICPHistAddress"/&gt;
 *         &lt;element name="myIcp" type="{urn:JadeWebServices/WSP_Registry2/}ICP"/&gt;
 *         &lt;element name="myMeteringHistory" type="{urn:JadeWebServices/WSP_Registry2/}ICPHistMetering2"/&gt;
 *         &lt;element name="myNetworkHistory" type="{urn:JadeWebServices/WSP_Registry2/}ICPHistNetwork"/&gt;
 *         &lt;element name="myPricingHistory" type="{urn:JadeWebServices/WSP_Registry2/}ICPHistPricing"/&gt;
 *         &lt;element name="myStatusHistory" type="{urn:JadeWebServices/WSP_Registry2/}ICPHistStatus"/&gt;
 *         &lt;element name="myTraderHistory" type="{urn:JadeWebServices/WSP_Registry2/}ICPHistRecon"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_ICPDetailsResponse", propOrder = {
    "myAddressHistory",
    "myIcp",
    "myMeteringHistory",
    "myNetworkHistory",
    "myPricingHistory",
    "myStatusHistory",
    "myTraderHistory"
})
public class WSICPDetailsResponse
    extends WSResponse
{

    @XmlElement(required = true, nillable = true)
    protected ICPHistAddress myAddressHistory;
    @XmlElement(required = true, nillable = true)
    protected ICP myIcp;
    @XmlElement(required = true, nillable = true)
    protected ICPHistMetering2 myMeteringHistory;
    @XmlElement(required = true, nillable = true)
    protected ICPHistNetwork myNetworkHistory;
    @XmlElement(required = true, nillable = true)
    protected ICPHistPricing myPricingHistory;
    @XmlElement(required = true, nillable = true)
    protected ICPHistStatus myStatusHistory;
    @XmlElement(required = true, nillable = true)
    protected ICPHistRecon myTraderHistory;

    /**
     * Gets the value of the myAddressHistory property.
     * 
     * @return
     *     possible object is
     *     {@link ICPHistAddress }
     *     
     */
    public ICPHistAddress getMyAddressHistory() {
        return myAddressHistory;
    }

    /**
     * Sets the value of the myAddressHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link ICPHistAddress }
     *     
     */
    public void setMyAddressHistory(ICPHistAddress value) {
        this.myAddressHistory = value;
    }

    /**
     * Gets the value of the myIcp property.
     * 
     * @return
     *     possible object is
     *     {@link ICP }
     *     
     */
    public ICP getMyIcp() {
        return myIcp;
    }

    /**
     * Sets the value of the myIcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link ICP }
     *     
     */
    public void setMyIcp(ICP value) {
        this.myIcp = value;
    }

    /**
     * Gets the value of the myMeteringHistory property.
     * 
     * @return
     *     possible object is
     *     {@link ICPHistMetering2 }
     *     
     */
    public ICPHistMetering2 getMyMeteringHistory() {
        return myMeteringHistory;
    }

    /**
     * Sets the value of the myMeteringHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link ICPHistMetering2 }
     *     
     */
    public void setMyMeteringHistory(ICPHistMetering2 value) {
        this.myMeteringHistory = value;
    }

    /**
     * Gets the value of the myNetworkHistory property.
     * 
     * @return
     *     possible object is
     *     {@link ICPHistNetwork }
     *     
     */
    public ICPHistNetwork getMyNetworkHistory() {
        return myNetworkHistory;
    }

    /**
     * Sets the value of the myNetworkHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link ICPHistNetwork }
     *     
     */
    public void setMyNetworkHistory(ICPHistNetwork value) {
        this.myNetworkHistory = value;
    }

    /**
     * Gets the value of the myPricingHistory property.
     * 
     * @return
     *     possible object is
     *     {@link ICPHistPricing }
     *     
     */
    public ICPHistPricing getMyPricingHistory() {
        return myPricingHistory;
    }

    /**
     * Sets the value of the myPricingHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link ICPHistPricing }
     *     
     */
    public void setMyPricingHistory(ICPHistPricing value) {
        this.myPricingHistory = value;
    }

    /**
     * Gets the value of the myStatusHistory property.
     * 
     * @return
     *     possible object is
     *     {@link ICPHistStatus }
     *     
     */
    public ICPHistStatus getMyStatusHistory() {
        return myStatusHistory;
    }

    /**
     * Sets the value of the myStatusHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link ICPHistStatus }
     *     
     */
    public void setMyStatusHistory(ICPHistStatus value) {
        this.myStatusHistory = value;
    }

    /**
     * Gets the value of the myTraderHistory property.
     * 
     * @return
     *     possible object is
     *     {@link ICPHistRecon }
     *     
     */
    public ICPHistRecon getMyTraderHistory() {
        return myTraderHistory;
    }

    /**
     * Sets the value of the myTraderHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link ICPHistRecon }
     *     
     */
    public void setMyTraderHistory(ICPHistRecon value) {
        this.myTraderHistory = value;
    }

}
