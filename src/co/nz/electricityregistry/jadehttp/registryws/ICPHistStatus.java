
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Status"
 * </dict-id>
 * <dict-description>
 * "This event type groups together the attributes relating to the pre-commissioning stage and energisation status of the ICP (see ICP status lifecycle in the Registry Functional Specification):
 * * Event Date
 * * ICP Status
 * * Status Reason
 * * User Reference"
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for ICPHistStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ICPHistStatus"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}ICPHistory"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="statusReason" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="vStatusText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ICPHistStatus", propOrder = {
    "status",
    "statusReason",
    "vStatusText"
})
public class ICPHistStatus
    extends ICPHistory
{

    protected Integer status;
    protected Integer statusReason;
    protected String vStatusText;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStatus(Integer value) {
        this.status = value;
    }

    /**
     * Gets the value of the statusReason property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStatusReason() {
        return statusReason;
    }

    /**
     * Sets the value of the statusReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStatusReason(Integer value) {
        this.statusReason = value;
    }

    /**
     * Gets the value of the vStatusText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVStatusText() {
        return vStatusText;
    }

    /**
     * Sets the value of the vStatusText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVStatusText(String value) {
        this.vStatusText = value;
    }

}
