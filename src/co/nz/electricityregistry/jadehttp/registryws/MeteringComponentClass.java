
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Metering Component"
 * </dict-id>
 * <dict-description>
 * "For attributes which apply to MeteringComponent and MeteringComponentSwitching"
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for MeteringComponentClass complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MeteringComponentClass"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}MeteringDetails"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allMeteringChannels" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfMeteringChannel"/&gt;
 *         &lt;element name="serialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeteringComponentClass", propOrder = {
    "allMeteringChannels",
    "serialNumber"
})
@XmlSeeAlso({
    MeteringComponent.class
})
public abstract class MeteringComponentClass
    extends MeteringDetails
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfMeteringChannel allMeteringChannels;
    protected String serialNumber;

    /**
     * Gets the value of the allMeteringChannels property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMeteringChannel }
     *     
     */
    public ArrayOfMeteringChannel getAllMeteringChannels() {
        return allMeteringChannels;
    }

    /**
     * Sets the value of the allMeteringChannels property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMeteringChannel }
     *     
     */
    public void setAllMeteringChannels(ArrayOfMeteringChannel value) {
        this.allMeteringChannels = value;
    }

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

}
