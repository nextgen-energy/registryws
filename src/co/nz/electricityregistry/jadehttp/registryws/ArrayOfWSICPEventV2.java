
package co.nz.electricityregistry.jadehttp.registryws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWS_ICPEvent_v2 complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWS_ICPEvent_v2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WS_ICPEvent_v2" type="{urn:JadeWebServices/WSP_Registry2/}WS_ICPEvent_v2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWS_ICPEvent_v2", propOrder = {
    "wsicpEventV2"
})
public class ArrayOfWSICPEventV2 {

    @XmlElement(name = "WS_ICPEvent_v2")
    protected List<WSICPEventV2> wsicpEventV2;

    /**
     * Gets the value of the wsicpEventV2 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsicpEventV2 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWSICPEventV2().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSICPEventV2 }
     * 
     * 
     */
    public List<WSICPEventV2> getWSICPEventV2() {
        if (wsicpEventV2 == null) {
            wsicpEventV2 = new ArrayList<WSICPEventV2>();
        }
        return this.wsicpEventV2;
    }

}
