
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PriceCategoryCode complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PriceCategoryCode"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}CodeInstance"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isAllocatedToIcp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="myNetwork" type="{urn:JadeWebServices/WSP_Registry2/}Participant"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceCategoryCode", propOrder = {
    "isActive",
    "isAllocatedToIcp",
    "myNetwork"
})
public class PriceCategoryCode
    extends CodeInstance
{

    protected Boolean isActive;
    protected Boolean isAllocatedToIcp;
    @XmlElement(required = true, nillable = true)
    protected Participant myNetwork;

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the isAllocatedToIcp property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAllocatedToIcp() {
        return isAllocatedToIcp;
    }

    /**
     * Sets the value of the isAllocatedToIcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAllocatedToIcp(Boolean value) {
        this.isAllocatedToIcp = value;
    }

    /**
     * Gets the value of the myNetwork property.
     * 
     * @return
     *     possible object is
     *     {@link Participant }
     *     
     */
    public Participant getMyNetwork() {
        return myNetwork;
    }

    /**
     * Sets the value of the myNetwork property.
     * 
     * @param value
     *     allowed object is
     *     {@link Participant }
     *     
     */
    public void setMyNetwork(Participant value) {
        this.myNetwork = value;
    }

}
