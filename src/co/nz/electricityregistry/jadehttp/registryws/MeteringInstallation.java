
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Metering Installation (Level 2)"
 * </dict-id>
 * <dict-description>
 * "An ICP may have multiple sites where metering is located.  Each site is identified by a unique Metering Installation Number.
 * Level 2 - Metering Installation Information Attributes:
 * * Metering Installation Number;
 * * Highest Metering Category;
 * * Metering Installation Location Code;
 * * ATH Participant Identifier;
 * * Metering Installation Type;
 * * Metering Installation Certification Type 
 * * Metering Installation Certification Date
 * * Metering Installation Certification Expiry Date;
 * * Control Device Certification Flag;
 * * Certification Variations;
 * * Certification Variations Expiry Date;
 * * Certification Number;
 * * Maximum Interrogation Cycle;
 * * Lease Price Code;
 * * Number Of Component Records;"
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for MeteringInstallation complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MeteringInstallation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}MeteringInstallationClass"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="certVariationsExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="certificationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="certificationExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="certificationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="controlDeviceCertificationFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="highestMeteringCategory" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="leasePriceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="locationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="maxInterrogationCycle" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="vATHparticipantIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vCertVariations" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vCertificationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vGPS_E" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="vGPS_N" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="vMeteringInstallationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vNumberOfComponents" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeteringInstallation", propOrder = {
    "certVariationsExpiryDate",
    "certificationDate",
    "certificationExpiryDate",
    "certificationNumber",
    "controlDeviceCertificationFlag",
    "highestMeteringCategory",
    "leasePriceCode",
    "locationCode",
    "maxInterrogationCycle",
    "vatHparticipantIdentifier",
    "vCertVariations",
    "vCertificationType",
    "vgpse",
    "vgpsn",
    "vMeteringInstallationType",
    "vNumberOfComponents"
})
public class MeteringInstallation
    extends MeteringInstallationClass
{

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar certVariationsExpiryDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar certificationDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar certificationExpiryDate;
    protected String certificationNumber;
    protected Boolean controlDeviceCertificationFlag;
    protected Integer highestMeteringCategory;
    protected String leasePriceCode;
    protected String locationCode;
    protected Integer maxInterrogationCycle;
    @XmlElement(name = "vATHparticipantIdentifier")
    protected String vatHparticipantIdentifier;
    protected String vCertVariations;
    protected String vCertificationType;
    @XmlElement(name = "vGPS_E")
    protected Double vgpse;
    @XmlElement(name = "vGPS_N")
    protected Double vgpsn;
    protected String vMeteringInstallationType;
    protected Integer vNumberOfComponents;

    /**
     * Gets the value of the certVariationsExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCertVariationsExpiryDate() {
        return certVariationsExpiryDate;
    }

    /**
     * Sets the value of the certVariationsExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCertVariationsExpiryDate(XMLGregorianCalendar value) {
        this.certVariationsExpiryDate = value;
    }

    /**
     * Gets the value of the certificationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCertificationDate() {
        return certificationDate;
    }

    /**
     * Sets the value of the certificationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCertificationDate(XMLGregorianCalendar value) {
        this.certificationDate = value;
    }

    /**
     * Gets the value of the certificationExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCertificationExpiryDate() {
        return certificationExpiryDate;
    }

    /**
     * Sets the value of the certificationExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCertificationExpiryDate(XMLGregorianCalendar value) {
        this.certificationExpiryDate = value;
    }

    /**
     * Gets the value of the certificationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificationNumber() {
        return certificationNumber;
    }

    /**
     * Sets the value of the certificationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificationNumber(String value) {
        this.certificationNumber = value;
    }

    /**
     * Gets the value of the controlDeviceCertificationFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isControlDeviceCertificationFlag() {
        return controlDeviceCertificationFlag;
    }

    /**
     * Sets the value of the controlDeviceCertificationFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setControlDeviceCertificationFlag(Boolean value) {
        this.controlDeviceCertificationFlag = value;
    }

    /**
     * Gets the value of the highestMeteringCategory property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHighestMeteringCategory() {
        return highestMeteringCategory;
    }

    /**
     * Sets the value of the highestMeteringCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHighestMeteringCategory(Integer value) {
        this.highestMeteringCategory = value;
    }

    /**
     * Gets the value of the leasePriceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeasePriceCode() {
        return leasePriceCode;
    }

    /**
     * Sets the value of the leasePriceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeasePriceCode(String value) {
        this.leasePriceCode = value;
    }

    /**
     * Gets the value of the locationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationCode() {
        return locationCode;
    }

    /**
     * Sets the value of the locationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationCode(String value) {
        this.locationCode = value;
    }

    /**
     * Gets the value of the maxInterrogationCycle property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxInterrogationCycle() {
        return maxInterrogationCycle;
    }

    /**
     * Sets the value of the maxInterrogationCycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxInterrogationCycle(Integer value) {
        this.maxInterrogationCycle = value;
    }

    /**
     * Gets the value of the vatHparticipantIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVATHparticipantIdentifier() {
        return vatHparticipantIdentifier;
    }

    /**
     * Sets the value of the vatHparticipantIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVATHparticipantIdentifier(String value) {
        this.vatHparticipantIdentifier = value;
    }

    /**
     * Gets the value of the vCertVariations property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVCertVariations() {
        return vCertVariations;
    }

    /**
     * Sets the value of the vCertVariations property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVCertVariations(String value) {
        this.vCertVariations = value;
    }

    /**
     * Gets the value of the vCertificationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVCertificationType() {
        return vCertificationType;
    }

    /**
     * Sets the value of the vCertificationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVCertificationType(String value) {
        this.vCertificationType = value;
    }

    /**
     * Gets the value of the vgpse property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getVGPSE() {
        return vgpse;
    }

    /**
     * Sets the value of the vgpse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setVGPSE(Double value) {
        this.vgpse = value;
    }

    /**
     * Gets the value of the vgpsn property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getVGPSN() {
        return vgpsn;
    }

    /**
     * Sets the value of the vgpsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setVGPSN(Double value) {
        this.vgpsn = value;
    }

    /**
     * Gets the value of the vMeteringInstallationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVMeteringInstallationType() {
        return vMeteringInstallationType;
    }

    /**
     * Sets the value of the vMeteringInstallationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVMeteringInstallationType(String value) {
        this.vMeteringInstallationType = value;
    }

    /**
     * Gets the value of the vNumberOfComponents property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVNumberOfComponents() {
        return vNumberOfComponents;
    }

    /**
     * Sets the value of the vNumberOfComponents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVNumberOfComponents(Integer value) {
        this.vNumberOfComponents = value;
    }

}
