
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_StatusNotify complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_StatusNotify"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_EventNotification"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="icpStatus" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="icpStatusReason" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_StatusNotify", propOrder = {
    "icpStatus",
    "icpStatusReason"
})
public class WSStatusNotify
    extends WSEventNotification
{

    protected Integer icpStatus;
    protected Integer icpStatusReason;

    /**
     * Gets the value of the icpStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIcpStatus() {
        return icpStatus;
    }

    /**
     * Sets the value of the icpStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIcpStatus(Integer value) {
        this.icpStatus = value;
    }

    /**
     * Gets the value of the icpStatusReason property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIcpStatusReason() {
        return icpStatusReason;
    }

    /**
     * Sets the value of the icpStatusReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIcpStatusReason(Integer value) {
        this.icpStatusReason = value;
    }

}
