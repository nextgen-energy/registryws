
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="userName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="unitOrNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="streetOrPropertyName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="streetOrPropertyFilter" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="suburbOrTown" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="isExactMatch" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="ownIcpsOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="commissionedOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userName",
    "password",
    "unitOrNumber",
    "streetOrPropertyName",
    "streetOrPropertyFilter",
    "suburbOrTown",
    "region",
    "isExactMatch",
    "ownIcpsOnly",
    "commissionedOnly"
})
@XmlRootElement(name = "icpSearch_v1")
public class IcpSearchV1 {

    @XmlElement(required = true)
    protected String userName;
    @XmlElement(required = true)
    protected String password;
    @XmlElement(required = true)
    protected String unitOrNumber;
    @XmlElement(required = true)
    protected String streetOrPropertyName;
    @XmlElement(required = true)
    protected String streetOrPropertyFilter;
    @XmlElement(required = true)
    protected String suburbOrTown;
    @XmlElement(required = true)
    protected String region;
    protected boolean isExactMatch;
    protected boolean ownIcpsOnly;
    protected boolean commissionedOnly;

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the unitOrNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitOrNumber() {
        return unitOrNumber;
    }

    /**
     * Sets the value of the unitOrNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitOrNumber(String value) {
        this.unitOrNumber = value;
    }

    /**
     * Gets the value of the streetOrPropertyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetOrPropertyName() {
        return streetOrPropertyName;
    }

    /**
     * Sets the value of the streetOrPropertyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetOrPropertyName(String value) {
        this.streetOrPropertyName = value;
    }

    /**
     * Gets the value of the streetOrPropertyFilter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetOrPropertyFilter() {
        return streetOrPropertyFilter;
    }

    /**
     * Sets the value of the streetOrPropertyFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetOrPropertyFilter(String value) {
        this.streetOrPropertyFilter = value;
    }

    /**
     * Gets the value of the suburbOrTown property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuburbOrTown() {
        return suburbOrTown;
    }

    /**
     * Sets the value of the suburbOrTown property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuburbOrTown(String value) {
        this.suburbOrTown = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Gets the value of the isExactMatch property.
     * 
     */
    public boolean isIsExactMatch() {
        return isExactMatch;
    }

    /**
     * Sets the value of the isExactMatch property.
     * 
     */
    public void setIsExactMatch(boolean value) {
        this.isExactMatch = value;
    }

    /**
     * Gets the value of the ownIcpsOnly property.
     * 
     */
    public boolean isOwnIcpsOnly() {
        return ownIcpsOnly;
    }

    /**
     * Sets the value of the ownIcpsOnly property.
     * 
     */
    public void setOwnIcpsOnly(boolean value) {
        this.ownIcpsOnly = value;
    }

    /**
     * Gets the value of the commissionedOnly property.
     * 
     */
    public boolean isCommissionedOnly() {
        return commissionedOnly;
    }

    /**
     * Sets the value of the commissionedOnly property.
     * 
     */
    public void setCommissionedOnly(boolean value) {
        this.commissionedOnly = value;
    }

}
