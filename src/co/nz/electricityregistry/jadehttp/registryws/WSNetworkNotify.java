
package co.nz.electricityregistry.jadehttp.registryws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WS_NetworkNotify complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_NetworkNotify"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_EventNotification"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dedicatedNSP" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="directBilledDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="directBilledStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fuelType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="generationCapacity" type="{urn:JadeWebServices/WSP_Registry2/}decimal_6_2" minOccurs="0"/&gt;
 *         &lt;element name="initialEnergisationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="installationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="networkParticipantIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="poc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="proposedTrader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="reconciliationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sharedICPList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="unmeteredLoadDetailsDistributor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_NetworkNotify", propOrder = {
    "dedicatedNSP",
    "directBilledDetails",
    "directBilledStatus",
    "fuelType",
    "generationCapacity",
    "initialEnergisationDate",
    "installationType",
    "networkParticipantIdentifier",
    "poc",
    "proposedTrader",
    "reconciliationType",
    "sharedICPList",
    "unmeteredLoadDetailsDistributor"
})
public class WSNetworkNotify
    extends WSEventNotification
{

    protected Boolean dedicatedNSP;
    protected String directBilledDetails;
    protected String directBilledStatus;
    protected String fuelType;
    protected BigDecimal generationCapacity;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar initialEnergisationDate;
    protected String installationType;
    protected String networkParticipantIdentifier;
    protected String poc;
    protected String proposedTrader;
    protected String reconciliationType;
    protected String sharedICPList;
    protected String unmeteredLoadDetailsDistributor;

    /**
     * Gets the value of the dedicatedNSP property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDedicatedNSP() {
        return dedicatedNSP;
    }

    /**
     * Sets the value of the dedicatedNSP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDedicatedNSP(Boolean value) {
        this.dedicatedNSP = value;
    }

    /**
     * Gets the value of the directBilledDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectBilledDetails() {
        return directBilledDetails;
    }

    /**
     * Sets the value of the directBilledDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectBilledDetails(String value) {
        this.directBilledDetails = value;
    }

    /**
     * Gets the value of the directBilledStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectBilledStatus() {
        return directBilledStatus;
    }

    /**
     * Sets the value of the directBilledStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectBilledStatus(String value) {
        this.directBilledStatus = value;
    }

    /**
     * Gets the value of the fuelType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelType() {
        return fuelType;
    }

    /**
     * Sets the value of the fuelType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelType(String value) {
        this.fuelType = value;
    }

    /**
     * Gets the value of the generationCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGenerationCapacity() {
        return generationCapacity;
    }

    /**
     * Sets the value of the generationCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGenerationCapacity(BigDecimal value) {
        this.generationCapacity = value;
    }

    /**
     * Gets the value of the initialEnergisationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInitialEnergisationDate() {
        return initialEnergisationDate;
    }

    /**
     * Sets the value of the initialEnergisationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInitialEnergisationDate(XMLGregorianCalendar value) {
        this.initialEnergisationDate = value;
    }

    /**
     * Gets the value of the installationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstallationType() {
        return installationType;
    }

    /**
     * Sets the value of the installationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstallationType(String value) {
        this.installationType = value;
    }

    /**
     * Gets the value of the networkParticipantIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkParticipantIdentifier() {
        return networkParticipantIdentifier;
    }

    /**
     * Sets the value of the networkParticipantIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkParticipantIdentifier(String value) {
        this.networkParticipantIdentifier = value;
    }

    /**
     * Gets the value of the poc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoc() {
        return poc;
    }

    /**
     * Sets the value of the poc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoc(String value) {
        this.poc = value;
    }

    /**
     * Gets the value of the proposedTrader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposedTrader() {
        return proposedTrader;
    }

    /**
     * Sets the value of the proposedTrader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposedTrader(String value) {
        this.proposedTrader = value;
    }

    /**
     * Gets the value of the reconciliationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReconciliationType() {
        return reconciliationType;
    }

    /**
     * Sets the value of the reconciliationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReconciliationType(String value) {
        this.reconciliationType = value;
    }

    /**
     * Gets the value of the sharedICPList property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSharedICPList() {
        return sharedICPList;
    }

    /**
     * Sets the value of the sharedICPList property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSharedICPList(String value) {
        this.sharedICPList = value;
    }

    /**
     * Gets the value of the unmeteredLoadDetailsDistributor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnmeteredLoadDetailsDistributor() {
        return unmeteredLoadDetailsDistributor;
    }

    /**
     * Sets the value of the unmeteredLoadDetailsDistributor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnmeteredLoadDetailsDistributor(String value) {
        this.unmeteredLoadDetailsDistributor = value;
    }

}
