
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_MeteringNotify complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_MeteringNotify"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_EventNotification"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allInstallations" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfWS_MeterInstallationNotify"/&gt;
 *         &lt;element name="amiFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="hhrFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="highestMeteringCategory" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="mepParticipantIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="meterChannelCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="meterMultiplierFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="nhhFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="numberOfInstallations" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="ppFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_MeteringNotify", propOrder = {
    "allInstallations",
    "amiFlag",
    "hhrFlag",
    "highestMeteringCategory",
    "mepParticipantIdentifier",
    "meterChannelCount",
    "meterMultiplierFlag",
    "nhhFlag",
    "numberOfInstallations",
    "ppFlag"
})
public class WSMeteringNotify
    extends WSEventNotification
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfWSMeterInstallationNotify allInstallations;
    protected Boolean amiFlag;
    protected Boolean hhrFlag;
    protected Integer highestMeteringCategory;
    protected String mepParticipantIdentifier;
    protected Integer meterChannelCount;
    protected Boolean meterMultiplierFlag;
    protected Boolean nhhFlag;
    protected Integer numberOfInstallations;
    protected Boolean ppFlag;

    /**
     * Gets the value of the allInstallations property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSMeterInstallationNotify }
     *     
     */
    public ArrayOfWSMeterInstallationNotify getAllInstallations() {
        return allInstallations;
    }

    /**
     * Sets the value of the allInstallations property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSMeterInstallationNotify }
     *     
     */
    public void setAllInstallations(ArrayOfWSMeterInstallationNotify value) {
        this.allInstallations = value;
    }

    /**
     * Gets the value of the amiFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAmiFlag() {
        return amiFlag;
    }

    /**
     * Sets the value of the amiFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAmiFlag(Boolean value) {
        this.amiFlag = value;
    }

    /**
     * Gets the value of the hhrFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHhrFlag() {
        return hhrFlag;
    }

    /**
     * Sets the value of the hhrFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHhrFlag(Boolean value) {
        this.hhrFlag = value;
    }

    /**
     * Gets the value of the highestMeteringCategory property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHighestMeteringCategory() {
        return highestMeteringCategory;
    }

    /**
     * Sets the value of the highestMeteringCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHighestMeteringCategory(Integer value) {
        this.highestMeteringCategory = value;
    }

    /**
     * Gets the value of the mepParticipantIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMepParticipantIdentifier() {
        return mepParticipantIdentifier;
    }

    /**
     * Sets the value of the mepParticipantIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMepParticipantIdentifier(String value) {
        this.mepParticipantIdentifier = value;
    }

    /**
     * Gets the value of the meterChannelCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMeterChannelCount() {
        return meterChannelCount;
    }

    /**
     * Sets the value of the meterChannelCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMeterChannelCount(Integer value) {
        this.meterChannelCount = value;
    }

    /**
     * Gets the value of the meterMultiplierFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMeterMultiplierFlag() {
        return meterMultiplierFlag;
    }

    /**
     * Sets the value of the meterMultiplierFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMeterMultiplierFlag(Boolean value) {
        this.meterMultiplierFlag = value;
    }

    /**
     * Gets the value of the nhhFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNhhFlag() {
        return nhhFlag;
    }

    /**
     * Sets the value of the nhhFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNhhFlag(Boolean value) {
        this.nhhFlag = value;
    }

    /**
     * Gets the value of the numberOfInstallations property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfInstallations() {
        return numberOfInstallations;
    }

    /**
     * Sets the value of the numberOfInstallations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfInstallations(Integer value) {
        this.numberOfInstallations = value;
    }

    /**
     * Gets the value of the ppFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPpFlag() {
        return ppFlag;
    }

    /**
     * Sets the value of the ppFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPpFlag(Boolean value) {
        this.ppFlag = value;
    }

}
