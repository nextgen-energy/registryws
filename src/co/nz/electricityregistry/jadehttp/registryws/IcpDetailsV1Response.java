
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="icpDetails_v1Result" type="{urn:JadeWebServices/WSP_Registry2/}WS_ICPDetailsResponse"/&gt;
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "icpDetailsV1Result",
    "message"
})
@XmlRootElement(name = "icpDetails_v1Response")
public class IcpDetailsV1Response {

    @XmlElement(name = "icpDetails_v1Result", required = true, nillable = true)
    protected WSICPDetailsResponse icpDetailsV1Result;
    @XmlElement(required = true)
    protected String message;

    /**
     * Gets the value of the icpDetailsV1Result property.
     * 
     * @return
     *     possible object is
     *     {@link WSICPDetailsResponse }
     *     
     */
    public WSICPDetailsResponse getIcpDetailsV1Result() {
        return icpDetailsV1Result;
    }

    /**
     * Sets the value of the icpDetailsV1Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSICPDetailsResponse }
     *     
     */
    public void setIcpDetailsV1Result(WSICPDetailsResponse value) {
        this.icpDetailsV1Result = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

}
