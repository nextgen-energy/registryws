
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_ICPEventsResponse_v2 complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_ICPEventsResponse_v2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_Response"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allEvents" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfWS_ICPEvent_v2"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_ICPEventsResponse_v2", propOrder = {
    "allEvents"
})
public class WSICPEventsResponseV2
    extends WSResponse
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfWSICPEventV2 allEvents;

    /**
     * Gets the value of the allEvents property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSICPEventV2 }
     *     
     */
    public ArrayOfWSICPEventV2 getAllEvents() {
        return allEvents;
    }

    /**
     * Sets the value of the allEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSICPEventV2 }
     *     
     */
    public void setAllEvents(ArrayOfWSICPEventV2 value) {
        this.allEvents = value;
    }

}
