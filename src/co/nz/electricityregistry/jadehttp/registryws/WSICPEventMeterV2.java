
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_ICPEventMeter_v2 complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_ICPEventMeter_v2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_ICPEvent_v2"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isAMINonComm" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isC_I_TOU" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_ICPEventMeter_v2", propOrder = {
    "isAMINonComm",
    "isCITOU"
})
public class WSICPEventMeterV2
    extends WSICPEventV2
{

    protected Boolean isAMINonComm;
    @XmlElement(name = "isC_I_TOU")
    protected Boolean isCITOU;

    /**
     * Gets the value of the isAMINonComm property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAMINonComm() {
        return isAMINonComm;
    }

    /**
     * Sets the value of the isAMINonComm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAMINonComm(Boolean value) {
        this.isAMINonComm = value;
    }

    /**
     * Gets the value of the isCITOU property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCITOU() {
        return isCITOU;
    }

    /**
     * Sets the value of the isCITOU property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCITOU(Boolean value) {
        this.isCITOU = value;
    }

}
