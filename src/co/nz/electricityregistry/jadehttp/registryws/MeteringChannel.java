
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Metering Channel (Level 4)"
 * </dict-id>
 * <dict-description>
 * "The registry holds details of all channels of each metering component of an ICP.  Each channel is assigned a sequential number that makes it unique within the component. 
 * Level 4 - Metering Channel Information Attributes:
 * * Metering Installation Number;
 * * Metering Component Serial Number;
 * * Channel Number;
 * * Number of Dials;
 * * Register Content Code;
 * * Period of Availability;
 * * Unit of Measurement;
 * * Energy Flow Direction;
 * * Accumulator Type;
 * * Settlement Indicator;
 * * Event Reading;"
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for MeteringChannel complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MeteringChannel"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}MeteringChannelClass"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accumulatorType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="energyFlowDirection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventReading" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numberOfDials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="periodOfAvailability" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="settlementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="vMeteringComponentSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vMeteringInstallationNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="vRegisterContentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vUnitOfMeasurement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeteringChannel", propOrder = {
    "accumulatorType",
    "energyFlowDirection",
    "eventReading",
    "numberOfDials",
    "periodOfAvailability",
    "settlementIndicator",
    "vMeteringComponentSerialNumber",
    "vMeteringInstallationNumber",
    "vRegisterContentCode",
    "vUnitOfMeasurement"
})
public class MeteringChannel
    extends MeteringChannelClass
{

    protected String accumulatorType;
    protected String energyFlowDirection;
    protected String eventReading;
    protected String numberOfDials;
    protected String periodOfAvailability;
    protected Boolean settlementIndicator;
    protected String vMeteringComponentSerialNumber;
    protected Integer vMeteringInstallationNumber;
    protected String vRegisterContentCode;
    protected String vUnitOfMeasurement;

    /**
     * Gets the value of the accumulatorType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccumulatorType() {
        return accumulatorType;
    }

    /**
     * Sets the value of the accumulatorType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccumulatorType(String value) {
        this.accumulatorType = value;
    }

    /**
     * Gets the value of the energyFlowDirection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnergyFlowDirection() {
        return energyFlowDirection;
    }

    /**
     * Sets the value of the energyFlowDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnergyFlowDirection(String value) {
        this.energyFlowDirection = value;
    }

    /**
     * Gets the value of the eventReading property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventReading() {
        return eventReading;
    }

    /**
     * Sets the value of the eventReading property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventReading(String value) {
        this.eventReading = value;
    }

    /**
     * Gets the value of the numberOfDials property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfDials() {
        return numberOfDials;
    }

    /**
     * Sets the value of the numberOfDials property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfDials(String value) {
        this.numberOfDials = value;
    }

    /**
     * Gets the value of the periodOfAvailability property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodOfAvailability() {
        return periodOfAvailability;
    }

    /**
     * Sets the value of the periodOfAvailability property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodOfAvailability(String value) {
        this.periodOfAvailability = value;
    }

    /**
     * Gets the value of the settlementIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSettlementIndicator() {
        return settlementIndicator;
    }

    /**
     * Sets the value of the settlementIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSettlementIndicator(Boolean value) {
        this.settlementIndicator = value;
    }

    /**
     * Gets the value of the vMeteringComponentSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVMeteringComponentSerialNumber() {
        return vMeteringComponentSerialNumber;
    }

    /**
     * Sets the value of the vMeteringComponentSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVMeteringComponentSerialNumber(String value) {
        this.vMeteringComponentSerialNumber = value;
    }

    /**
     * Gets the value of the vMeteringInstallationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVMeteringInstallationNumber() {
        return vMeteringInstallationNumber;
    }

    /**
     * Sets the value of the vMeteringInstallationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVMeteringInstallationNumber(Integer value) {
        this.vMeteringInstallationNumber = value;
    }

    /**
     * Gets the value of the vRegisterContentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVRegisterContentCode() {
        return vRegisterContentCode;
    }

    /**
     * Sets the value of the vRegisterContentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVRegisterContentCode(String value) {
        this.vRegisterContentCode = value;
    }

    /**
     * Gets the value of the vUnitOfMeasurement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUnitOfMeasurement() {
        return vUnitOfMeasurement;
    }

    /**
     * Sets the value of the vUnitOfMeasurement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUnitOfMeasurement(String value) {
        this.vUnitOfMeasurement = value;
    }

}
