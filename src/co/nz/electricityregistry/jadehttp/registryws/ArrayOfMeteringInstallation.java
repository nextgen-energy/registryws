
package co.nz.electricityregistry.jadehttp.registryws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfMeteringInstallation complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfMeteringInstallation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MeteringInstallation" type="{urn:JadeWebServices/WSP_Registry2/}MeteringInstallation" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfMeteringInstallation", propOrder = {
    "meteringInstallation"
})
public class ArrayOfMeteringInstallation {

    @XmlElement(name = "MeteringInstallation")
    protected List<MeteringInstallation> meteringInstallation;

    /**
     * Gets the value of the meteringInstallation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the meteringInstallation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMeteringInstallation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MeteringInstallation }
     * 
     * 
     */
    public List<MeteringInstallation> getMeteringInstallation() {
        if (meteringInstallation == null) {
            meteringInstallation = new ArrayList<MeteringInstallation>();
        }
        return this.meteringInstallation;
    }

}
