
package co.nz.electricityregistry.jadehttp.registryws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWS_ICPSearchResult complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWS_ICPSearchResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WS_ICPSearchResult" type="{urn:JadeWebServices/WSP_Registry2/}WS_ICPSearchResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWS_ICPSearchResult", propOrder = {
    "wsicpSearchResult"
})
public class ArrayOfWSICPSearchResult {

    @XmlElement(name = "WS_ICPSearchResult")
    protected List<WSICPSearchResult> wsicpSearchResult;

    /**
     * Gets the value of the wsicpSearchResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsicpSearchResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWSICPSearchResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSICPSearchResult }
     * 
     * 
     */
    public List<WSICPSearchResult> getWSICPSearchResult() {
        if (wsicpSearchResult == null) {
            wsicpSearchResult = new ArrayList<WSICPSearchResult>();
        }
        return this.wsicpSearchResult;
    }

}
