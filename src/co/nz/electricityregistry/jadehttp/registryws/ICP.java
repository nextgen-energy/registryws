
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *  -	ICP id's are: 10 alphas + 2 alphas + 3 hexadecimal (ie 'code' + 'hex')
 * 
 *  -	For validation, two external method calls are used, in conjunction with fcs12.dll in the Jade bin directory
 * 
 *  	1: makeChecksum(code:String):String  
 *  			Adds 3 characters 'hex', on the end of a (10 alpha + 2 alpha) 'code'
 *  			It will make a checksum for any length input
 *  	
 *  	2: characters 11&12 are unique to a Network and are validated against GXP Reconciliation table prior to 
 *  		ICP creation by interface H or on-line (new for enhanced registry 2002)
 *  			validated by validICPCreate returns true if these 2 chars are available to network to use
 * 
 * 	2: validChecksum(icp:String):Boolean  
 * 		Returns true if last 3 chars on any input is correct checksum
 * 
 *  -	These methods may be called from anywhere in the schema	
 * 	see JadeScript:  testChecksumCode();
 * 
 * 	OR from methods on global - global.icpChecksum - accepts a string and returns a boolean
 * 								global.generateChecksum - accepts and returns a string
 * 
 * <p>Java class for ICP complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ICP"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}Object"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="icpId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="switchInProgress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="switchInProgressMEP" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ICP", propOrder = {
    "icpId",
    "switchInProgress",
    "switchInProgressMEP"
})
public class ICP
    extends Object
{

    protected String icpId;
    protected Boolean switchInProgress;
    protected Boolean switchInProgressMEP;

    /**
     * Gets the value of the icpId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcpId() {
        return icpId;
    }

    /**
     * Sets the value of the icpId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcpId(String value) {
        this.icpId = value;
    }

    /**
     * Gets the value of the switchInProgress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSwitchInProgress() {
        return switchInProgress;
    }

    /**
     * Sets the value of the switchInProgress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSwitchInProgress(Boolean value) {
        this.switchInProgress = value;
    }

    /**
     * Gets the value of the switchInProgressMEP property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSwitchInProgressMEP() {
        return switchInProgressMEP;
    }

    /**
     * Sets the value of the switchInProgressMEP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSwitchInProgressMEP(Boolean value) {
        this.switchInProgressMEP = value;
    }

}
