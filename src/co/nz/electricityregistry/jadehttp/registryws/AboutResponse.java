
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="aboutResult" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;element name="systemName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="dateTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="functionList" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="icpSearchDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="icpDetailsDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="icpEventsDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "aboutResult",
    "systemName",
    "dateTime",
    "functionList",
    "icpSearchDescription",
    "icpDetailsDescription",
    "icpEventsDescription"
})
@XmlRootElement(name = "aboutResponse")
public class AboutResponse {

    @XmlElement(required = true)
    protected java.lang.Object aboutResult;
    @XmlElement(required = true)
    protected String systemName;
    @XmlElement(required = true)
    protected String dateTime;
    @XmlElement(required = true)
    protected String functionList;
    @XmlElement(required = true)
    protected String icpSearchDescription;
    @XmlElement(required = true)
    protected String icpDetailsDescription;
    @XmlElement(required = true)
    protected String icpEventsDescription;

    /**
     * Gets the value of the aboutResult property.
     * 
     * @return
     *     possible object is
     *     {@link java.lang.Object }
     *     
     */
    public java.lang.Object getAboutResult() {
        return aboutResult;
    }

    /**
     * Sets the value of the aboutResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link java.lang.Object }
     *     
     */
    public void setAboutResult(java.lang.Object value) {
        this.aboutResult = value;
    }

    /**
     * Gets the value of the systemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemName() {
        return systemName;
    }

    /**
     * Sets the value of the systemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemName(String value) {
        this.systemName = value;
    }

    /**
     * Gets the value of the dateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * Sets the value of the dateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTime(String value) {
        this.dateTime = value;
    }

    /**
     * Gets the value of the functionList property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFunctionList() {
        return functionList;
    }

    /**
     * Sets the value of the functionList property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFunctionList(String value) {
        this.functionList = value;
    }

    /**
     * Gets the value of the icpSearchDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcpSearchDescription() {
        return icpSearchDescription;
    }

    /**
     * Sets the value of the icpSearchDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcpSearchDescription(String value) {
        this.icpSearchDescription = value;
    }

    /**
     * Gets the value of the icpDetailsDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcpDetailsDescription() {
        return icpDetailsDescription;
    }

    /**
     * Sets the value of the icpDetailsDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcpDetailsDescription(String value) {
        this.icpDetailsDescription = value;
    }

    /**
     * Gets the value of the icpEventsDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcpEventsDescription() {
        return icpEventsDescription;
    }

    /**
     * Sets the value of the icpEventsDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcpEventsDescription(String value) {
        this.icpEventsDescription = value;
    }

}
