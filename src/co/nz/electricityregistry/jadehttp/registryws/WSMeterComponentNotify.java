
package co.nz.electricityregistry.jadehttp.registryws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WS_MeterComponentNotify complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_MeterComponentNotify"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_EventNotificationParent"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allChannels" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfWS_MeterChannelNotify"/&gt;
 *         &lt;element name="amiFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="compensationfactor" type="{urn:JadeWebServices/WSP_Registry2/}decimal_9_3" minOccurs="0"/&gt;
 *         &lt;element name="meterType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="meteringComponentSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="meteringComponentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="meteringInstallationCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="meteringInstallationNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="numberOfChannels" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="removalDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_MeterComponentNotify", propOrder = {
    "allChannels",
    "amiFlag",
    "compensationfactor",
    "meterType",
    "meteringComponentSerialNumber",
    "meteringComponentType",
    "meteringInstallationCategory",
    "meteringInstallationNumber",
    "numberOfChannels",
    "owner",
    "removalDate"
})
public class WSMeterComponentNotify
    extends WSEventNotificationParent
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfWSMeterChannelNotify allChannels;
    protected Boolean amiFlag;
    protected BigDecimal compensationfactor;
    protected String meterType;
    protected String meteringComponentSerialNumber;
    protected String meteringComponentType;
    protected String meteringInstallationCategory;
    protected Integer meteringInstallationNumber;
    protected Integer numberOfChannels;
    protected String owner;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar removalDate;

    /**
     * Gets the value of the allChannels property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSMeterChannelNotify }
     *     
     */
    public ArrayOfWSMeterChannelNotify getAllChannels() {
        return allChannels;
    }

    /**
     * Sets the value of the allChannels property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSMeterChannelNotify }
     *     
     */
    public void setAllChannels(ArrayOfWSMeterChannelNotify value) {
        this.allChannels = value;
    }

    /**
     * Gets the value of the amiFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAmiFlag() {
        return amiFlag;
    }

    /**
     * Sets the value of the amiFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAmiFlag(Boolean value) {
        this.amiFlag = value;
    }

    /**
     * Gets the value of the compensationfactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCompensationfactor() {
        return compensationfactor;
    }

    /**
     * Sets the value of the compensationfactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCompensationfactor(BigDecimal value) {
        this.compensationfactor = value;
    }

    /**
     * Gets the value of the meterType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeterType() {
        return meterType;
    }

    /**
     * Sets the value of the meterType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeterType(String value) {
        this.meterType = value;
    }

    /**
     * Gets the value of the meteringComponentSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeteringComponentSerialNumber() {
        return meteringComponentSerialNumber;
    }

    /**
     * Sets the value of the meteringComponentSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeteringComponentSerialNumber(String value) {
        this.meteringComponentSerialNumber = value;
    }

    /**
     * Gets the value of the meteringComponentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeteringComponentType() {
        return meteringComponentType;
    }

    /**
     * Sets the value of the meteringComponentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeteringComponentType(String value) {
        this.meteringComponentType = value;
    }

    /**
     * Gets the value of the meteringInstallationCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeteringInstallationCategory() {
        return meteringInstallationCategory;
    }

    /**
     * Sets the value of the meteringInstallationCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeteringInstallationCategory(String value) {
        this.meteringInstallationCategory = value;
    }

    /**
     * Gets the value of the meteringInstallationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMeteringInstallationNumber() {
        return meteringInstallationNumber;
    }

    /**
     * Sets the value of the meteringInstallationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMeteringInstallationNumber(Integer value) {
        this.meteringInstallationNumber = value;
    }

    /**
     * Gets the value of the numberOfChannels property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfChannels() {
        return numberOfChannels;
    }

    /**
     * Sets the value of the numberOfChannels property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfChannels(Integer value) {
        this.numberOfChannels = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * Gets the value of the removalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRemovalDate() {
        return removalDate;
    }

    /**
     * Sets the value of the removalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRemovalDate(XMLGregorianCalendar value) {
        this.removalDate = value;
    }

}
