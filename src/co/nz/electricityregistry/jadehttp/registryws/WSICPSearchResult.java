
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_ICPSearchResult complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_ICPSearchResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_WebServiceTransients"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="myAddressHistory" type="{urn:JadeWebServices/WSP_Registry2/}ICPHistAddress"/&gt;
 *         &lt;element name="myIcp" type="{urn:JadeWebServices/WSP_Registry2/}ICP"/&gt;
 *         &lt;element name="myStatusHistory" type="{urn:JadeWebServices/WSP_Registry2/}ICPHistStatus"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_ICPSearchResult", propOrder = {
    "myAddressHistory",
    "myIcp",
    "myStatusHistory"
})
public class WSICPSearchResult
    extends WSWebServiceTransients
{

    @XmlElement(required = true, nillable = true)
    protected ICPHistAddress myAddressHistory;
    @XmlElement(required = true, nillable = true)
    protected ICP myIcp;
    @XmlElement(required = true, nillable = true)
    protected ICPHistStatus myStatusHistory;

    /**
     * Gets the value of the myAddressHistory property.
     * 
     * @return
     *     possible object is
     *     {@link ICPHistAddress }
     *     
     */
    public ICPHistAddress getMyAddressHistory() {
        return myAddressHistory;
    }

    /**
     * Sets the value of the myAddressHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link ICPHistAddress }
     *     
     */
    public void setMyAddressHistory(ICPHistAddress value) {
        this.myAddressHistory = value;
    }

    /**
     * Gets the value of the myIcp property.
     * 
     * @return
     *     possible object is
     *     {@link ICP }
     *     
     */
    public ICP getMyIcp() {
        return myIcp;
    }

    /**
     * Sets the value of the myIcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link ICP }
     *     
     */
    public void setMyIcp(ICP value) {
        this.myIcp = value;
    }

    /**
     * Gets the value of the myStatusHistory property.
     * 
     * @return
     *     possible object is
     *     {@link ICPHistStatus }
     *     
     */
    public ICPHistStatus getMyStatusHistory() {
        return myStatusHistory;
    }

    /**
     * Sets the value of the myStatusHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link ICPHistStatus }
     *     
     */
    public void setMyStatusHistory(ICPHistStatus value) {
        this.myStatusHistory = value;
    }

}
