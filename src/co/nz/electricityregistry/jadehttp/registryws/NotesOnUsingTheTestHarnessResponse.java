
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="notesOnUsingTheTestHarnessResult" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "notesOnUsingTheTestHarnessResult",
    "notes"
})
@XmlRootElement(name = "notesOnUsingTheTestHarnessResponse")
public class NotesOnUsingTheTestHarnessResponse {

    @XmlElement(required = true)
    protected java.lang.Object notesOnUsingTheTestHarnessResult;
    @XmlElement(required = true)
    protected String notes;

    /**
     * Gets the value of the notesOnUsingTheTestHarnessResult property.
     * 
     * @return
     *     possible object is
     *     {@link java.lang.Object }
     *     
     */
    public java.lang.Object getNotesOnUsingTheTestHarnessResult() {
        return notesOnUsingTheTestHarnessResult;
    }

    /**
     * Sets the value of the notesOnUsingTheTestHarnessResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link java.lang.Object }
     *     
     */
    public void setNotesOnUsingTheTestHarnessResult(java.lang.Object value) {
        this.notesOnUsingTheTestHarnessResult = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

}
