
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WS_ICPNotificationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_ICPNotificationResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_Response"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allNotifications" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfWS_EventNotification"/&gt;
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="notificationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="resumptionPoint" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_ICPNotificationResponse", propOrder = {
    "allNotifications",
    "message",
    "notificationDate",
    "resumptionPoint"
})
public class WSICPNotificationResponse
    extends WSResponse
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfWSEventNotification allNotifications;
    protected String message;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar notificationDate;
    protected Integer resumptionPoint;

    /**
     * Gets the value of the allNotifications property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSEventNotification }
     *     
     */
    public ArrayOfWSEventNotification getAllNotifications() {
        return allNotifications;
    }

    /**
     * Sets the value of the allNotifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSEventNotification }
     *     
     */
    public void setAllNotifications(ArrayOfWSEventNotification value) {
        this.allNotifications = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the notificationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNotificationDate() {
        return notificationDate;
    }

    /**
     * Sets the value of the notificationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNotificationDate(XMLGregorianCalendar value) {
        this.notificationDate = value;
    }

    /**
     * Gets the value of the resumptionPoint property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResumptionPoint() {
        return resumptionPoint;
    }

    /**
     * Sets the value of the resumptionPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResumptionPoint(Integer value) {
        this.resumptionPoint = value;
    }

}
