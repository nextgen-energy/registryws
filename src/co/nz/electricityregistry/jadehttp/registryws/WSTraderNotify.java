
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_TraderNotify complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_TraderNotify"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_EventNotification"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="anzsic" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dailyUnmeteredkWh" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="profile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="proposedMEPParticipantIdentifer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="submissionTypeHHR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="submissionTypeNHH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="trader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="unmFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="unmeteredLoadDetailsTrader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_TraderNotify", propOrder = {
    "anzsic",
    "dailyUnmeteredkWh",
    "profile",
    "proposedMEPParticipantIdentifer",
    "submissionTypeHHR",
    "submissionTypeNHH",
    "trader",
    "unmFlag",
    "unmeteredLoadDetailsTrader"
})
public class WSTraderNotify
    extends WSEventNotification
{

    protected String anzsic;
    protected String dailyUnmeteredkWh;
    protected String profile;
    protected String proposedMEPParticipantIdentifer;
    protected String submissionTypeHHR;
    protected String submissionTypeNHH;
    protected String trader;
    protected String unmFlag;
    protected String unmeteredLoadDetailsTrader;

    /**
     * Gets the value of the anzsic property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnzsic() {
        return anzsic;
    }

    /**
     * Sets the value of the anzsic property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnzsic(String value) {
        this.anzsic = value;
    }

    /**
     * Gets the value of the dailyUnmeteredkWh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDailyUnmeteredkWh() {
        return dailyUnmeteredkWh;
    }

    /**
     * Sets the value of the dailyUnmeteredkWh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDailyUnmeteredkWh(String value) {
        this.dailyUnmeteredkWh = value;
    }

    /**
     * Gets the value of the profile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfile() {
        return profile;
    }

    /**
     * Sets the value of the profile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfile(String value) {
        this.profile = value;
    }

    /**
     * Gets the value of the proposedMEPParticipantIdentifer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposedMEPParticipantIdentifer() {
        return proposedMEPParticipantIdentifer;
    }

    /**
     * Sets the value of the proposedMEPParticipantIdentifer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposedMEPParticipantIdentifer(String value) {
        this.proposedMEPParticipantIdentifer = value;
    }

    /**
     * Gets the value of the submissionTypeHHR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubmissionTypeHHR() {
        return submissionTypeHHR;
    }

    /**
     * Sets the value of the submissionTypeHHR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubmissionTypeHHR(String value) {
        this.submissionTypeHHR = value;
    }

    /**
     * Gets the value of the submissionTypeNHH property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubmissionTypeNHH() {
        return submissionTypeNHH;
    }

    /**
     * Sets the value of the submissionTypeNHH property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubmissionTypeNHH(String value) {
        this.submissionTypeNHH = value;
    }

    /**
     * Gets the value of the trader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrader() {
        return trader;
    }

    /**
     * Sets the value of the trader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrader(String value) {
        this.trader = value;
    }

    /**
     * Gets the value of the unmFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnmFlag() {
        return unmFlag;
    }

    /**
     * Sets the value of the unmFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnmFlag(String value) {
        this.unmFlag = value;
    }

    /**
     * Gets the value of the unmeteredLoadDetailsTrader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnmeteredLoadDetailsTrader() {
        return unmeteredLoadDetailsTrader;
    }

    /**
     * Sets the value of the unmeteredLoadDetailsTrader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnmeteredLoadDetailsTrader(String value) {
        this.unmeteredLoadDetailsTrader = value;
    }

}
