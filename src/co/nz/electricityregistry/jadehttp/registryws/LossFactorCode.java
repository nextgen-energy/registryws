
package co.nz.electricityregistry.jadehttp.registryws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LossFactorCode complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LossFactorCode"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}CodeInstance"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="endPeriod" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="lossFactorConsumption" type="{urn:JadeWebServices/WSP_Registry2/}decimal_5_4" minOccurs="0"/&gt;
 *         &lt;element name="lossFactorGeneration" type="{urn:JadeWebServices/WSP_Registry2/}decimal_5_4" minOccurs="0"/&gt;
 *         &lt;element name="myNetwork" type="{urn:JadeWebServices/WSP_Registry2/}Participant"/&gt;
 *         &lt;element name="startPeriod" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LossFactorCode", propOrder = {
    "endPeriod",
    "lossFactorConsumption",
    "lossFactorGeneration",
    "myNetwork",
    "startPeriod"
})
public class LossFactorCode
    extends CodeInstance
{

    protected Integer endPeriod;
    protected BigDecimal lossFactorConsumption;
    protected BigDecimal lossFactorGeneration;
    @XmlElement(required = true, nillable = true)
    protected Participant myNetwork;
    protected Integer startPeriod;

    /**
     * Gets the value of the endPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEndPeriod() {
        return endPeriod;
    }

    /**
     * Sets the value of the endPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEndPeriod(Integer value) {
        this.endPeriod = value;
    }

    /**
     * Gets the value of the lossFactorConsumption property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLossFactorConsumption() {
        return lossFactorConsumption;
    }

    /**
     * Sets the value of the lossFactorConsumption property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLossFactorConsumption(BigDecimal value) {
        this.lossFactorConsumption = value;
    }

    /**
     * Gets the value of the lossFactorGeneration property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLossFactorGeneration() {
        return lossFactorGeneration;
    }

    /**
     * Sets the value of the lossFactorGeneration property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLossFactorGeneration(BigDecimal value) {
        this.lossFactorGeneration = value;
    }

    /**
     * Gets the value of the myNetwork property.
     * 
     * @return
     *     possible object is
     *     {@link Participant }
     *     
     */
    public Participant getMyNetwork() {
        return myNetwork;
    }

    /**
     * Sets the value of the myNetwork property.
     * 
     * @param value
     *     allowed object is
     *     {@link Participant }
     *     
     */
    public void setMyNetwork(Participant value) {
        this.myNetwork = value;
    }

    /**
     * Gets the value of the startPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStartPeriod() {
        return startPeriod;
    }

    /**
     * Sets the value of the startPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStartPeriod(Integer value) {
        this.startPeriod = value;
    }

}
