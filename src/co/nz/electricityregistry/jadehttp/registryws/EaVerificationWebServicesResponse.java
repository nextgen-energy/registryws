
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="eaVerificationWebServicesResult" type="{urn:JadeWebServices/WSP_Registry2/}WS_eaVerificationWebServicetResponse"/&gt;
 *         &lt;element name="_message" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "eaVerificationWebServicesResult",
    "message"
})
@XmlRootElement(name = "eaVerificationWebServicesResponse")
public class EaVerificationWebServicesResponse {

    @XmlElement(required = true, nillable = true)
    protected WSEaVerificationWebServicetResponse eaVerificationWebServicesResult;
    @XmlElement(name = "_message", required = true)
    protected String message;

    /**
     * Gets the value of the eaVerificationWebServicesResult property.
     * 
     * @return
     *     possible object is
     *     {@link WSEaVerificationWebServicetResponse }
     *     
     */
    public WSEaVerificationWebServicetResponse getEaVerificationWebServicesResult() {
        return eaVerificationWebServicesResult;
    }

    /**
     * Sets the value of the eaVerificationWebServicesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSEaVerificationWebServicetResponse }
     *     
     */
    public void setEaVerificationWebServicesResult(WSEaVerificationWebServicetResponse value) {
        this.eaVerificationWebServicesResult = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

}
