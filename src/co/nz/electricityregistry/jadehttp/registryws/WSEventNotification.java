
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * CR-1202 Smart Notifications
 * Jade will provide a WSDL allowing participants to build a client application to call a notifications web service. The daily batch process that generates the notifications will remain unchanged.
 * To access notifications; 
 *  1.	A participant will poll the Registry requesting that either outstanding (unsent) notifications are sent, or previously sent notifications are resent.
 *  2.	The Registry will send a response, the response will contain either:
 * "	a batch of notifications; or
 * "	an "all notifications sent" response.
 * A participant may continue to poll the Registry requesting notifications are sent until an "all notifications sent" response is received.
 * 
 * 
 * <p>Java class for WS_EventNotification complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_EventNotification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_EventNotificationParent"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="aaTransactionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="auditNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="eventDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="icpIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="notificationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="switchStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="userReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_EventNotification", propOrder = {
    "aaTransactionType",
    "auditNumber",
    "creationDate",
    "eventDate",
    "icpIdentifier",
    "notificationType",
    "switchStatus",
    "userReference"
})
@XmlSeeAlso({
    WSAddressNotify.class,
    WSMeteringNotify.class,
    WSNetworkNotify.class,
    WSPricingNotify.class,
    WSStatusNotify.class,
    WSTraderNotify.class
})
public abstract class WSEventNotification
    extends WSEventNotificationParent
{

    protected String aaTransactionType;
    protected String auditNumber;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar eventDate;
    protected String icpIdentifier;
    protected String notificationType;
    protected String switchStatus;
    protected String userReference;

    /**
     * Gets the value of the aaTransactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAaTransactionType() {
        return aaTransactionType;
    }

    /**
     * Sets the value of the aaTransactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAaTransactionType(String value) {
        this.aaTransactionType = value;
    }

    /**
     * Gets the value of the auditNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuditNumber() {
        return auditNumber;
    }

    /**
     * Sets the value of the auditNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuditNumber(String value) {
        this.auditNumber = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the eventDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventDate() {
        return eventDate;
    }

    /**
     * Sets the value of the eventDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventDate(XMLGregorianCalendar value) {
        this.eventDate = value;
    }

    /**
     * Gets the value of the icpIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcpIdentifier() {
        return icpIdentifier;
    }

    /**
     * Sets the value of the icpIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcpIdentifier(String value) {
        this.icpIdentifier = value;
    }

    /**
     * Gets the value of the notificationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotificationType() {
        return notificationType;
    }

    /**
     * Sets the value of the notificationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotificationType(String value) {
        this.notificationType = value;
    }

    /**
     * Gets the value of the switchStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwitchStatus() {
        return switchStatus;
    }

    /**
     * Sets the value of the switchStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwitchStatus(String value) {
        this.switchStatus = value;
    }

    /**
     * Gets the value of the userReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserReference() {
        return userReference;
    }

    /**
     * Sets the value of the userReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserReference(String value) {
        this.userReference = value;
    }

}
