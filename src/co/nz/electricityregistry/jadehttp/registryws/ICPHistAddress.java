
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Address"
 * </dict-id>
 * <dict-description>
 * "* Event Date;
 * * Physical Address Unit;
 * * Physical Address Property name;
 * * Physical Address Number;
 * * Physical Address Street;
 * * Physical Address Suburb;
 * * Physical Address Town;
 * * Physical Address Region;
 * * Physical Address Post Code;
 * * GPS_X;
 * * GPS_Y and
 * * User Reference.
 * The completion and maintenance of an address in the Registry is the responsibility of the Distributor.  The industry has developed guidelines for the consistent population of addresses.  These guidelines are contained in Appendix 2 of the Registry Functional Specification.
 * 
 * The minimum information required for a valid address is:
 * * Physical Address Property name or Physical Address Street;
 * * Physical Address Town or Physical Address Suburb; and
 * * Physical Address Region."
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for ICPHistAddress complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ICPHistAddress"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}ICPHistory"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="postCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="propertyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="suburb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="town" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="unit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vGPS_Easting" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="vGPS_Northing" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="vRegion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ICPHistAddress", propOrder = {
    "number",
    "postCode",
    "propertyName",
    "street",
    "suburb",
    "town",
    "unit",
    "vgpsEasting",
    "vgpsNorthing",
    "vRegion"
})
public class ICPHistAddress
    extends ICPHistory
{

    protected String number;
    protected Integer postCode;
    protected String propertyName;
    protected String street;
    protected String suburb;
    protected String town;
    protected String unit;
    @XmlElement(name = "vGPS_Easting")
    protected Double vgpsEasting;
    @XmlElement(name = "vGPS_Northing")
    protected Double vgpsNorthing;
    protected String vRegion;

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Gets the value of the postCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPostCode() {
        return postCode;
    }

    /**
     * Sets the value of the postCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPostCode(Integer value) {
        this.postCode = value;
    }

    /**
     * Gets the value of the propertyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * Sets the value of the propertyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyName(String value) {
        this.propertyName = value;
    }

    /**
     * Gets the value of the street property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the value of the street property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Gets the value of the suburb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuburb() {
        return suburb;
    }

    /**
     * Sets the value of the suburb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuburb(String value) {
        this.suburb = value;
    }

    /**
     * Gets the value of the town property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTown() {
        return town;
    }

    /**
     * Sets the value of the town property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTown(String value) {
        this.town = value;
    }

    /**
     * Gets the value of the unit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets the value of the unit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnit(String value) {
        this.unit = value;
    }

    /**
     * Gets the value of the vgpsEasting property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getVGPSEasting() {
        return vgpsEasting;
    }

    /**
     * Sets the value of the vgpsEasting property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setVGPSEasting(Double value) {
        this.vgpsEasting = value;
    }

    /**
     * Gets the value of the vgpsNorthing property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getVGPSNorthing() {
        return vgpsNorthing;
    }

    /**
     * Sets the value of the vgpsNorthing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setVGPSNorthing(Double value) {
        this.vgpsNorthing = value;
    }

    /**
     * Gets the value of the vRegion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVRegion() {
        return vRegion;
    }

    /**
     * Sets the value of the vRegion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVRegion(String value) {
        this.vRegion = value;
    }

}
