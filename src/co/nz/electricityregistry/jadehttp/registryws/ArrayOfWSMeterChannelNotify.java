
package co.nz.electricityregistry.jadehttp.registryws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWS_MeterChannelNotify complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWS_MeterChannelNotify"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WS_MeterChannelNotify" type="{urn:JadeWebServices/WSP_Registry2/}WS_MeterChannelNotify" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWS_MeterChannelNotify", propOrder = {
    "wsMeterChannelNotify"
})
public class ArrayOfWSMeterChannelNotify {

    @XmlElement(name = "WS_MeterChannelNotify")
    protected List<WSMeterChannelNotify> wsMeterChannelNotify;

    /**
     * Gets the value of the wsMeterChannelNotify property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsMeterChannelNotify property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWSMeterChannelNotify().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSMeterChannelNotify }
     * 
     * 
     */
    public List<WSMeterChannelNotify> getWSMeterChannelNotify() {
        if (wsMeterChannelNotify == null) {
            wsMeterChannelNotify = new ArrayList<WSMeterChannelNotify>();
        }
        return this.wsMeterChannelNotify;
    }

}
