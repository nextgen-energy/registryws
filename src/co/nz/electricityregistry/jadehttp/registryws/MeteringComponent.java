
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Metering Component (Level 3)"
 * </dict-id>
 * <dict-description>
 * "At each installation or site within an ICP, information on each metering component located there is recorded in the registry and each is identified by the component's own physical serial number or one assigned by the MEP that makes it unique within the ICP.
 * Level 3 - Metering Component Information Attributes:
 * * Metering Installation Number;
 * * Metering Component Serial Number;
 * * Metering Component Type;
 * * Meter Type;
 * * AMI Flag;
 * * Metering Installation Category;
 * * Compensation Factor;
 * * Owner;
 * * Number Of Channel Records;"
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for MeteringComponent complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MeteringComponent"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}MeteringComponentClass"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="amiFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="compensationFactor" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="meterType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="meteringCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="removalDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="vComponentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vMeteringInstallationNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="vNumberOfChannels" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeteringComponent", propOrder = {
    "amiFlag",
    "compensationFactor",
    "meterType",
    "meteringCategory",
    "owner",
    "removalDate",
    "vComponentType",
    "vMeteringInstallationNumber",
    "vNumberOfChannels"
})
public class MeteringComponent
    extends MeteringComponentClass
{

    protected Boolean amiFlag;
    protected Double compensationFactor;
    protected String meterType;
    protected String meteringCategory;
    protected String owner;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar removalDate;
    protected String vComponentType;
    protected Integer vMeteringInstallationNumber;
    protected Integer vNumberOfChannels;

    /**
     * Gets the value of the amiFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAmiFlag() {
        return amiFlag;
    }

    /**
     * Sets the value of the amiFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAmiFlag(Boolean value) {
        this.amiFlag = value;
    }

    /**
     * Gets the value of the compensationFactor property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCompensationFactor() {
        return compensationFactor;
    }

    /**
     * Sets the value of the compensationFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCompensationFactor(Double value) {
        this.compensationFactor = value;
    }

    /**
     * Gets the value of the meterType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeterType() {
        return meterType;
    }

    /**
     * Sets the value of the meterType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeterType(String value) {
        this.meterType = value;
    }

    /**
     * Gets the value of the meteringCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeteringCategory() {
        return meteringCategory;
    }

    /**
     * Sets the value of the meteringCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeteringCategory(String value) {
        this.meteringCategory = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * Gets the value of the removalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRemovalDate() {
        return removalDate;
    }

    /**
     * Sets the value of the removalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRemovalDate(XMLGregorianCalendar value) {
        this.removalDate = value;
    }

    /**
     * Gets the value of the vComponentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVComponentType() {
        return vComponentType;
    }

    /**
     * Sets the value of the vComponentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVComponentType(String value) {
        this.vComponentType = value;
    }

    /**
     * Gets the value of the vMeteringInstallationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVMeteringInstallationNumber() {
        return vMeteringInstallationNumber;
    }

    /**
     * Sets the value of the vMeteringInstallationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVMeteringInstallationNumber(Integer value) {
        this.vMeteringInstallationNumber = value;
    }

    /**
     * Gets the value of the vNumberOfChannels property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVNumberOfChannels() {
        return vNumberOfChannels;
    }

    /**
     * Sets the value of the vNumberOfChannels property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVNumberOfChannels(Integer value) {
        this.vNumberOfChannels = value;
    }

}
