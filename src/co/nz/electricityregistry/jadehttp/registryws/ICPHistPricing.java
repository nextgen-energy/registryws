
package co.nz.electricityregistry.jadehttp.registryws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Pricing"
 * </dict-id>
 * <dict-description>
 * "This event type groups together the attributes relating to Distributor line charges and loss factors:
 * * Event Date;
 * * Distributor Price Category Code;
 * * Distributor Loss Category Code;
 * * Distributor Installation Details;
 * * Chargeable Capacity; and
 * * User Reference."
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for ICPHistPricing complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ICPHistPricing"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}ICPHistory"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allPriceCategoryCodes" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfPriceCategoryCode"/&gt;
 *         &lt;element name="chargeableCapacity" type="{urn:JadeWebServices/WSP_Registry2/}decimal_9_2" minOccurs="0"/&gt;
 *         &lt;element name="installDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="myLossFactorCode" type="{urn:JadeWebServices/WSP_Registry2/}LossFactorCode"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ICPHistPricing", propOrder = {
    "allPriceCategoryCodes",
    "chargeableCapacity",
    "installDetails",
    "myLossFactorCode"
})
public class ICPHistPricing
    extends ICPHistory
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfPriceCategoryCode allPriceCategoryCodes;
    protected BigDecimal chargeableCapacity;
    protected String installDetails;
    @XmlElement(required = true, nillable = true)
    protected LossFactorCode myLossFactorCode;

    /**
     * Gets the value of the allPriceCategoryCodes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPriceCategoryCode }
     *     
     */
    public ArrayOfPriceCategoryCode getAllPriceCategoryCodes() {
        return allPriceCategoryCodes;
    }

    /**
     * Sets the value of the allPriceCategoryCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPriceCategoryCode }
     *     
     */
    public void setAllPriceCategoryCodes(ArrayOfPriceCategoryCode value) {
        this.allPriceCategoryCodes = value;
    }

    /**
     * Gets the value of the chargeableCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChargeableCapacity() {
        return chargeableCapacity;
    }

    /**
     * Sets the value of the chargeableCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChargeableCapacity(BigDecimal value) {
        this.chargeableCapacity = value;
    }

    /**
     * Gets the value of the installDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstallDetails() {
        return installDetails;
    }

    /**
     * Sets the value of the installDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstallDetails(String value) {
        this.installDetails = value;
    }

    /**
     * Gets the value of the myLossFactorCode property.
     * 
     * @return
     *     possible object is
     *     {@link LossFactorCode }
     *     
     */
    public LossFactorCode getMyLossFactorCode() {
        return myLossFactorCode;
    }

    /**
     * Sets the value of the myLossFactorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link LossFactorCode }
     *     
     */
    public void setMyLossFactorCode(LossFactorCode value) {
        this.myLossFactorCode = value;
    }

}
