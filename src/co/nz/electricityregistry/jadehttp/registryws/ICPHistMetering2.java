
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <xml>
 * <dict-id>
 * "Metering"
 * </dict-id>
 * <dict-description>
 * "This event type groups together the attributes relating to the physical meters at the ICP site and for identifying the Metering Equipment Provider;
 * This top level provides a snapshot of some of the metering attributes detailed in the lower levels of metering information as well as the Metering Equipment Provider Identifier of the participant responsible for maintaining this information in the registry.
 * Level 1 - Metering Summary Attributes:
 * * Event Date;
 * * MEP Participant Identifier;
 * * Highest Metering Category;
 * * HHR Flag;
 * * NHH Flag;
 * * PP Flag;
 * * AMI Flag;
 * * Meter Register Count;
 * * Meter Multiplier Flag; 
 * * Number Of Metering Installation Information Records; and
 * * User Reference.
 * It is possible to have more than one type of metering (NHH, HHR, PP) installed (although an ICP can be unmetered) at an ICP. At least one must be recorded against an ICP.
 * * If the site is totally unmetered then no metering information will be recorded for the ICP within the Metering event.  Information about unmetered load is maintained in the Trader event only.
 * An ICP may have multiple sites where metering is located.  Each site is identified by a unique Metering Installation Number.
 * Level 2 - Metering Installation Information Attributes:
 * * Metering Installation Number;
 * * Highest Metering Category;
 * * Metering Installation Location Code;
 * * ATH Participant Identifier;
 * * Metering Installation Type;
 * * Metering Installation Certification Type 
 * * Metering Installation Certification Date
 * * Metering Installation Certification Expiry Date;
 * * Control Device Certification Flag;
 * * Certification Variations;
 * * Certification Variations Expiry Date;
 * * Certification Number;
 * * Maximum Interrogation Cycle;
 * * Lease Price Code;
 * * Number Of Component Records;
 * * 
 * At each installation or site within an ICP, information on each metering component located there is recorded in the registry and each is identified by the component's own physical serial number or one assigned by the MEP that makes it unique within the ICP.
 * Level 3 - Metering Component Information Attributes:
 * * Metering Installation Number;
 * * Metering Component Serial Number;
 * * Metering Component Type;
 * * Meter Type;
 * * AMI Flag;
 * * Metering Category;
 * * Compensation Factor;
 * * Owner;
 * * Number Of Channel Records;
 * The registry holds details of all channels of each metering component of an ICP.  Each channel is assigned a sequential number that makes it unique within the component. 
 * Level 4 - Metering Channel Information Attributes:
 * * Metering Installation Number;
 * * Metering Component Serial Number;
 * * Channel Number;
 * * Number of Dials;
 * * Register Content Code;
 * * Period of Availability;
 * * Unit of Measurement;
 * * Energy Flow Direction;
 * * Accumulator Type;
 * * Settlement Indicator;
 * * Event Reading;"
 * </dict-description>
 * </xml>
 * 
 * <p>Java class for ICPHistMetering2 complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ICPHistMetering2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}ICPHistory"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allMeteringInstallations" type="{urn:JadeWebServices/WSP_Registry2/}ArrayOfMeteringInstallation"/&gt;
 *         &lt;element name="amiFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="hhrFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="highestMeteringCategory" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="meterMultiplierFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="meterRegisterCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="myMEP" type="{urn:JadeWebServices/WSP_Registry2/}Participant"/&gt;
 *         &lt;element name="nhhFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ppFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="vMEPidentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vNumberOfInstallations" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ICPHistMetering2", propOrder = {
    "allMeteringInstallations",
    "amiFlag",
    "hhrFlag",
    "highestMeteringCategory",
    "meterMultiplierFlag",
    "meterRegisterCount",
    "myMEP",
    "nhhFlag",
    "ppFlag",
    "vmePidentifier",
    "vNumberOfInstallations"
})
public class ICPHistMetering2
    extends ICPHistory
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfMeteringInstallation allMeteringInstallations;
    protected Boolean amiFlag;
    protected Boolean hhrFlag;
    protected Integer highestMeteringCategory;
    protected Boolean meterMultiplierFlag;
    protected Integer meterRegisterCount;
    @XmlElement(required = true, nillable = true)
    protected Participant myMEP;
    protected Boolean nhhFlag;
    protected Boolean ppFlag;
    @XmlElement(name = "vMEPidentifier")
    protected String vmePidentifier;
    protected Integer vNumberOfInstallations;

    /**
     * Gets the value of the allMeteringInstallations property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMeteringInstallation }
     *     
     */
    public ArrayOfMeteringInstallation getAllMeteringInstallations() {
        return allMeteringInstallations;
    }

    /**
     * Sets the value of the allMeteringInstallations property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMeteringInstallation }
     *     
     */
    public void setAllMeteringInstallations(ArrayOfMeteringInstallation value) {
        this.allMeteringInstallations = value;
    }

    /**
     * Gets the value of the amiFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAmiFlag() {
        return amiFlag;
    }

    /**
     * Sets the value of the amiFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAmiFlag(Boolean value) {
        this.amiFlag = value;
    }

    /**
     * Gets the value of the hhrFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHhrFlag() {
        return hhrFlag;
    }

    /**
     * Sets the value of the hhrFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHhrFlag(Boolean value) {
        this.hhrFlag = value;
    }

    /**
     * Gets the value of the highestMeteringCategory property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHighestMeteringCategory() {
        return highestMeteringCategory;
    }

    /**
     * Sets the value of the highestMeteringCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHighestMeteringCategory(Integer value) {
        this.highestMeteringCategory = value;
    }

    /**
     * Gets the value of the meterMultiplierFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMeterMultiplierFlag() {
        return meterMultiplierFlag;
    }

    /**
     * Sets the value of the meterMultiplierFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMeterMultiplierFlag(Boolean value) {
        this.meterMultiplierFlag = value;
    }

    /**
     * Gets the value of the meterRegisterCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMeterRegisterCount() {
        return meterRegisterCount;
    }

    /**
     * Sets the value of the meterRegisterCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMeterRegisterCount(Integer value) {
        this.meterRegisterCount = value;
    }

    /**
     * Gets the value of the myMEP property.
     * 
     * @return
     *     possible object is
     *     {@link Participant }
     *     
     */
    public Participant getMyMEP() {
        return myMEP;
    }

    /**
     * Sets the value of the myMEP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Participant }
     *     
     */
    public void setMyMEP(Participant value) {
        this.myMEP = value;
    }

    /**
     * Gets the value of the nhhFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNhhFlag() {
        return nhhFlag;
    }

    /**
     * Sets the value of the nhhFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNhhFlag(Boolean value) {
        this.nhhFlag = value;
    }

    /**
     * Gets the value of the ppFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPpFlag() {
        return ppFlag;
    }

    /**
     * Sets the value of the ppFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPpFlag(Boolean value) {
        this.ppFlag = value;
    }

    /**
     * Gets the value of the vmePidentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVMEPidentifier() {
        return vmePidentifier;
    }

    /**
     * Sets the value of the vmePidentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVMEPidentifier(String value) {
        this.vmePidentifier = value;
    }

    /**
     * Gets the value of the vNumberOfInstallations property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVNumberOfInstallations() {
        return vNumberOfInstallations;
    }

    /**
     * Sets the value of the vNumberOfInstallations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVNumberOfInstallations(Integer value) {
        this.vNumberOfInstallations = value;
    }

}
