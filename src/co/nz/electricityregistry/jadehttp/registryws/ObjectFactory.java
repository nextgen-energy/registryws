
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.nz.electricityregistry.jadehttp.registryws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.nz.electricityregistry.jadehttp.registryws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link About }
     * 
     */
    public About createAbout() {
        return new About();
    }

    /**
     * Create an instance of {@link AboutResponse }
     * 
     */
    public AboutResponse createAboutResponse() {
        return new AboutResponse();
    }

    /**
     * Create an instance of {@link EaVerificationWebServices }
     * 
     */
    public EaVerificationWebServices createEaVerificationWebServices() {
        return new EaVerificationWebServices();
    }

    /**
     * Create an instance of {@link EaVerificationWebServicesResponse }
     * 
     */
    public EaVerificationWebServicesResponse createEaVerificationWebServicesResponse() {
        return new EaVerificationWebServicesResponse();
    }

    /**
     * Create an instance of {@link WSEaVerificationWebServicetResponse }
     * 
     */
    public WSEaVerificationWebServicetResponse createWSEaVerificationWebServicetResponse() {
        return new WSEaVerificationWebServicetResponse();
    }

    /**
     * Create an instance of {@link IcpDetailsV1 }
     * 
     */
    public IcpDetailsV1 createIcpDetailsV1() {
        return new IcpDetailsV1();
    }

    /**
     * Create an instance of {@link IcpDetailsV1Response }
     * 
     */
    public IcpDetailsV1Response createIcpDetailsV1Response() {
        return new IcpDetailsV1Response();
    }

    /**
     * Create an instance of {@link WSICPDetailsResponse }
     * 
     */
    public WSICPDetailsResponse createWSICPDetailsResponse() {
        return new WSICPDetailsResponse();
    }

    /**
     * Create an instance of {@link IcpEventsV1 }
     * 
     */
    public IcpEventsV1 createIcpEventsV1() {
        return new IcpEventsV1();
    }

    /**
     * Create an instance of {@link IcpEventsV1Response }
     * 
     */
    public IcpEventsV1Response createIcpEventsV1Response() {
        return new IcpEventsV1Response();
    }

    /**
     * Create an instance of {@link WSICPEventsResponse }
     * 
     */
    public WSICPEventsResponse createWSICPEventsResponse() {
        return new WSICPEventsResponse();
    }

    /**
     * Create an instance of {@link IcpEventsV2 }
     * 
     */
    public IcpEventsV2 createIcpEventsV2() {
        return new IcpEventsV2();
    }

    /**
     * Create an instance of {@link IcpEventsV2Response }
     * 
     */
    public IcpEventsV2Response createIcpEventsV2Response() {
        return new IcpEventsV2Response();
    }

    /**
     * Create an instance of {@link WSICPEventsResponseV2 }
     * 
     */
    public WSICPEventsResponseV2 createWSICPEventsResponseV2() {
        return new WSICPEventsResponseV2();
    }

    /**
     * Create an instance of {@link IcpMeterIdSearchV1 }
     * 
     */
    public IcpMeterIdSearchV1 createIcpMeterIdSearchV1() {
        return new IcpMeterIdSearchV1();
    }

    /**
     * Create an instance of {@link IcpMeterIdSearchV1Response }
     * 
     */
    public IcpMeterIdSearchV1Response createIcpMeterIdSearchV1Response() {
        return new IcpMeterIdSearchV1Response();
    }

    /**
     * Create an instance of {@link WSICPSearchResponse }
     * 
     */
    public WSICPSearchResponse createWSICPSearchResponse() {
        return new WSICPSearchResponse();
    }

    /**
     * Create an instance of {@link IcpNotificationsV1 }
     * 
     */
    public IcpNotificationsV1 createIcpNotificationsV1() {
        return new IcpNotificationsV1();
    }

    /**
     * Create an instance of {@link IcpNotificationsV1Response }
     * 
     */
    public IcpNotificationsV1Response createIcpNotificationsV1Response() {
        return new IcpNotificationsV1Response();
    }

    /**
     * Create an instance of {@link WSICPNotificationResponse }
     * 
     */
    public WSICPNotificationResponse createWSICPNotificationResponse() {
        return new WSICPNotificationResponse();
    }

    /**
     * Create an instance of {@link IcpSearchV1 }
     * 
     */
    public IcpSearchV1 createIcpSearchV1() {
        return new IcpSearchV1();
    }

    /**
     * Create an instance of {@link IcpSearchV1Response }
     * 
     */
    public IcpSearchV1Response createIcpSearchV1Response() {
        return new IcpSearchV1Response();
    }

    /**
     * Create an instance of {@link NotesOnUsingTheTestHarness }
     * 
     */
    public NotesOnUsingTheTestHarness createNotesOnUsingTheTestHarness() {
        return new NotesOnUsingTheTestHarness();
    }

    /**
     * Create an instance of {@link NotesOnUsingTheTestHarnessResponse }
     * 
     */
    public NotesOnUsingTheTestHarnessResponse createNotesOnUsingTheTestHarnessResponse() {
        return new NotesOnUsingTheTestHarnessResponse();
    }

    /**
     * Create an instance of {@link WSWebServiceTransients }
     * 
     */
    public WSWebServiceTransients createWSWebServiceTransients() {
        return new WSWebServiceTransients();
    }

    /**
     * Create an instance of {@link ArrayOfWSError }
     * 
     */
    public ArrayOfWSError createArrayOfWSError() {
        return new ArrayOfWSError();
    }

    /**
     * Create an instance of {@link WSError }
     * 
     */
    public WSError createWSError() {
        return new WSError();
    }

    /**
     * Create an instance of {@link WSMessage }
     * 
     */
    public WSMessage createWSMessage() {
        return new WSMessage();
    }

    /**
     * Create an instance of {@link ICPHistAddress }
     * 
     */
    public ICPHistAddress createICPHistAddress() {
        return new ICPHistAddress();
    }

    /**
     * Create an instance of {@link ICP }
     * 
     */
    public ICP createICP() {
        return new ICP();
    }

    /**
     * Create an instance of {@link ICPHistMetering2 }
     * 
     */
    public ICPHistMetering2 createICPHistMetering2() {
        return new ICPHistMetering2();
    }

    /**
     * Create an instance of {@link ICPHistNetwork }
     * 
     */
    public ICPHistNetwork createICPHistNetwork() {
        return new ICPHistNetwork();
    }

    /**
     * Create an instance of {@link ICPHistPricing }
     * 
     */
    public ICPHistPricing createICPHistPricing() {
        return new ICPHistPricing();
    }

    /**
     * Create an instance of {@link ICPHistStatus }
     * 
     */
    public ICPHistStatus createICPHistStatus() {
        return new ICPHistStatus();
    }

    /**
     * Create an instance of {@link ICPHistRecon }
     * 
     */
    public ICPHistRecon createICPHistRecon() {
        return new ICPHistRecon();
    }

    /**
     * Create an instance of {@link ArrayOfMeteringInstallation }
     * 
     */
    public ArrayOfMeteringInstallation createArrayOfMeteringInstallation() {
        return new ArrayOfMeteringInstallation();
    }

    /**
     * Create an instance of {@link Participant }
     * 
     */
    public Participant createParticipant() {
        return new Participant();
    }

    /**
     * Create an instance of {@link ArrayOfICP }
     * 
     */
    public ArrayOfICP createArrayOfICP() {
        return new ArrayOfICP();
    }

    /**
     * Create an instance of {@link NSPMapping }
     * 
     */
    public NSPMapping createNSPMapping() {
        return new NSPMapping();
    }

    /**
     * Create an instance of {@link ArrayOfPriceCategoryCode }
     * 
     */
    public ArrayOfPriceCategoryCode createArrayOfPriceCategoryCode() {
        return new ArrayOfPriceCategoryCode();
    }

    /**
     * Create an instance of {@link LossFactorCode }
     * 
     */
    public LossFactorCode createLossFactorCode() {
        return new LossFactorCode();
    }

    /**
     * Create an instance of {@link CodeInstance }
     * 
     */
    public CodeInstance createCodeInstance() {
        return new CodeInstance();
    }

    /**
     * Create an instance of {@link ArrayOfRetailerProfile }
     * 
     */
    public ArrayOfRetailerProfile createArrayOfRetailerProfile() {
        return new ArrayOfRetailerProfile();
    }

    /**
     * Create an instance of {@link MeteringInstallation }
     * 
     */
    public MeteringInstallation createMeteringInstallation() {
        return new MeteringInstallation();
    }

    /**
     * Create an instance of {@link PriceCategoryCode }
     * 
     */
    public PriceCategoryCode createPriceCategoryCode() {
        return new PriceCategoryCode();
    }

    /**
     * Create an instance of {@link CodeDefinition }
     * 
     */
    public CodeDefinition createCodeDefinition() {
        return new CodeDefinition();
    }

    /**
     * Create an instance of {@link RetailerProfile }
     * 
     */
    public RetailerProfile createRetailerProfile() {
        return new RetailerProfile();
    }

    /**
     * Create an instance of {@link ArrayOfMeteringComponent }
     * 
     */
    public ArrayOfMeteringComponent createArrayOfMeteringComponent() {
        return new ArrayOfMeteringComponent();
    }

    /**
     * Create an instance of {@link Profile }
     * 
     */
    public Profile createProfile() {
        return new Profile();
    }

    /**
     * Create an instance of {@link SortRetailerProfile }
     * 
     */
    public SortRetailerProfile createSortRetailerProfile() {
        return new SortRetailerProfile();
    }

    /**
     * Create an instance of {@link MeteringComponent }
     * 
     */
    public MeteringComponent createMeteringComponent() {
        return new MeteringComponent();
    }

    /**
     * Create an instance of {@link ArrayOfMeteringChannel }
     * 
     */
    public ArrayOfMeteringChannel createArrayOfMeteringChannel() {
        return new ArrayOfMeteringChannel();
    }

    /**
     * Create an instance of {@link MeteringChannel }
     * 
     */
    public MeteringChannel createMeteringChannel() {
        return new MeteringChannel();
    }

    /**
     * Create an instance of {@link ArrayOfWSICPEvent }
     * 
     */
    public ArrayOfWSICPEvent createArrayOfWSICPEvent() {
        return new ArrayOfWSICPEvent();
    }

    /**
     * Create an instance of {@link WSICPEvent }
     * 
     */
    public WSICPEvent createWSICPEvent() {
        return new WSICPEvent();
    }

    /**
     * Create an instance of {@link ArrayOfWSICPEventV2 }
     * 
     */
    public ArrayOfWSICPEventV2 createArrayOfWSICPEventV2() {
        return new ArrayOfWSICPEventV2();
    }

    /**
     * Create an instance of {@link WSICPEventV2 }
     * 
     */
    public WSICPEventV2 createWSICPEventV2() {
        return new WSICPEventV2();
    }

    /**
     * Create an instance of {@link WSICPEventMeterV2 }
     * 
     */
    public WSICPEventMeterV2 createWSICPEventMeterV2() {
        return new WSICPEventMeterV2();
    }

    /**
     * Create an instance of {@link ArrayOfWSICPSearchResult }
     * 
     */
    public ArrayOfWSICPSearchResult createArrayOfWSICPSearchResult() {
        return new ArrayOfWSICPSearchResult();
    }

    /**
     * Create an instance of {@link WSICPSearchResult }
     * 
     */
    public WSICPSearchResult createWSICPSearchResult() {
        return new WSICPSearchResult();
    }

    /**
     * Create an instance of {@link ArrayOfWSEventNotification }
     * 
     */
    public ArrayOfWSEventNotification createArrayOfWSEventNotification() {
        return new ArrayOfWSEventNotification();
    }

    /**
     * Create an instance of {@link WSAddressNotify }
     * 
     */
    public WSAddressNotify createWSAddressNotify() {
        return new WSAddressNotify();
    }

    /**
     * Create an instance of {@link WSMeteringNotify }
     * 
     */
    public WSMeteringNotify createWSMeteringNotify() {
        return new WSMeteringNotify();
    }

    /**
     * Create an instance of {@link WSNetworkNotify }
     * 
     */
    public WSNetworkNotify createWSNetworkNotify() {
        return new WSNetworkNotify();
    }

    /**
     * Create an instance of {@link WSPricingNotify }
     * 
     */
    public WSPricingNotify createWSPricingNotify() {
        return new WSPricingNotify();
    }

    /**
     * Create an instance of {@link WSStatusNotify }
     * 
     */
    public WSStatusNotify createWSStatusNotify() {
        return new WSStatusNotify();
    }

    /**
     * Create an instance of {@link WSTraderNotify }
     * 
     */
    public WSTraderNotify createWSTraderNotify() {
        return new WSTraderNotify();
    }

    /**
     * Create an instance of {@link ArrayOfWSMeterInstallationNotify }
     * 
     */
    public ArrayOfWSMeterInstallationNotify createArrayOfWSMeterInstallationNotify() {
        return new ArrayOfWSMeterInstallationNotify();
    }

    /**
     * Create an instance of {@link WSMeterInstallationNotify }
     * 
     */
    public WSMeterInstallationNotify createWSMeterInstallationNotify() {
        return new WSMeterInstallationNotify();
    }

    /**
     * Create an instance of {@link ArrayOfWSMeterComponentNotify }
     * 
     */
    public ArrayOfWSMeterComponentNotify createArrayOfWSMeterComponentNotify() {
        return new ArrayOfWSMeterComponentNotify();
    }

    /**
     * Create an instance of {@link WSMeterComponentNotify }
     * 
     */
    public WSMeterComponentNotify createWSMeterComponentNotify() {
        return new WSMeterComponentNotify();
    }

    /**
     * Create an instance of {@link ArrayOfWSMeterChannelNotify }
     * 
     */
    public ArrayOfWSMeterChannelNotify createArrayOfWSMeterChannelNotify() {
        return new ArrayOfWSMeterChannelNotify();
    }

    /**
     * Create an instance of {@link WSMeterChannelNotify }
     * 
     */
    public WSMeterChannelNotify createWSMeterChannelNotify() {
        return new WSMeterChannelNotify();
    }

}
