
package co.nz.electricityregistry.jadehttp.registryws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WS_MeterChannelNotify complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WS_MeterChannelNotify"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:JadeWebServices/WSP_Registry2/}WS_EventNotificationParent"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accumulatorType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="channelNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="energyFlowDirection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventReading" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="meteringComponentSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="meteringInstallationNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="numberOfDials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="periodOfAvailability" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="registerContentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="settlementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="unitOfMeasurement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_MeterChannelNotify", propOrder = {
    "accumulatorType",
    "channelNumber",
    "energyFlowDirection",
    "eventReading",
    "meteringComponentSerialNumber",
    "meteringInstallationNumber",
    "numberOfDials",
    "periodOfAvailability",
    "registerContentCode",
    "settlementIndicator",
    "unitOfMeasurement"
})
public class WSMeterChannelNotify
    extends WSEventNotificationParent
{

    protected String accumulatorType;
    protected Integer channelNumber;
    protected String energyFlowDirection;
    protected String eventReading;
    protected String meteringComponentSerialNumber;
    protected Integer meteringInstallationNumber;
    protected String numberOfDials;
    protected String periodOfAvailability;
    protected String registerContentCode;
    protected Boolean settlementIndicator;
    protected String unitOfMeasurement;

    /**
     * Gets the value of the accumulatorType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccumulatorType() {
        return accumulatorType;
    }

    /**
     * Sets the value of the accumulatorType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccumulatorType(String value) {
        this.accumulatorType = value;
    }

    /**
     * Gets the value of the channelNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChannelNumber() {
        return channelNumber;
    }

    /**
     * Sets the value of the channelNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChannelNumber(Integer value) {
        this.channelNumber = value;
    }

    /**
     * Gets the value of the energyFlowDirection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnergyFlowDirection() {
        return energyFlowDirection;
    }

    /**
     * Sets the value of the energyFlowDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnergyFlowDirection(String value) {
        this.energyFlowDirection = value;
    }

    /**
     * Gets the value of the eventReading property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventReading() {
        return eventReading;
    }

    /**
     * Sets the value of the eventReading property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventReading(String value) {
        this.eventReading = value;
    }

    /**
     * Gets the value of the meteringComponentSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeteringComponentSerialNumber() {
        return meteringComponentSerialNumber;
    }

    /**
     * Sets the value of the meteringComponentSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeteringComponentSerialNumber(String value) {
        this.meteringComponentSerialNumber = value;
    }

    /**
     * Gets the value of the meteringInstallationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMeteringInstallationNumber() {
        return meteringInstallationNumber;
    }

    /**
     * Sets the value of the meteringInstallationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMeteringInstallationNumber(Integer value) {
        this.meteringInstallationNumber = value;
    }

    /**
     * Gets the value of the numberOfDials property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfDials() {
        return numberOfDials;
    }

    /**
     * Sets the value of the numberOfDials property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfDials(String value) {
        this.numberOfDials = value;
    }

    /**
     * Gets the value of the periodOfAvailability property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodOfAvailability() {
        return periodOfAvailability;
    }

    /**
     * Sets the value of the periodOfAvailability property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodOfAvailability(String value) {
        this.periodOfAvailability = value;
    }

    /**
     * Gets the value of the registerContentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterContentCode() {
        return registerContentCode;
    }

    /**
     * Sets the value of the registerContentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterContentCode(String value) {
        this.registerContentCode = value;
    }

    /**
     * Gets the value of the settlementIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSettlementIndicator() {
        return settlementIndicator;
    }

    /**
     * Sets the value of the settlementIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSettlementIndicator(Boolean value) {
        this.settlementIndicator = value;
    }

    /**
     * Gets the value of the unitOfMeasurement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    /**
     * Sets the value of the unitOfMeasurement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitOfMeasurement(String value) {
        this.unitOfMeasurement = value;
    }

}
