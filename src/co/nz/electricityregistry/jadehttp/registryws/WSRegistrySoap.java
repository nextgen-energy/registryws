
package co.nz.electricityregistry.jadehttp.registryws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.3.1-SNAPSHOT
 * Generated source version: 2.2
 * 
 */
@WebService(name = "WSRegistrySoap", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface WSRegistrySoap {


    /**
     * 
     * @param dateTime
     * @param functionList
     * @param systemName
     * @param icpDetailsDescription
     * @param icpEventsDescription
     * @param aboutResult
     * @param icpSearchDescription
     */
    @WebMethod(action = "urn:JadeWebServices/WSP_Registry2/about")
    @RequestWrapper(localName = "about", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.About")
    @ResponseWrapper(localName = "aboutResponse", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.AboutResponse")
    public void about(
        @WebParam(name = "aboutResult", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<java.lang.Object> aboutResult,
        @WebParam(name = "systemName", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> systemName,
        @WebParam(name = "dateTime", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> dateTime,
        @WebParam(name = "functionList", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> functionList,
        @WebParam(name = "icpSearchDescription", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> icpSearchDescription,
        @WebParam(name = "icpDetailsDescription", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> icpDetailsDescription,
        @WebParam(name = "icpEventsDescription", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> icpEventsDescription);

    /**
     * 
     * @param notesOnUsingTheTestHarnessResult
     * @param notes
     */
    @WebMethod(action = "urn:JadeWebServices/WSP_Registry2/notesOnUsingTheTestHarness")
    @RequestWrapper(localName = "notesOnUsingTheTestHarness", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.NotesOnUsingTheTestHarness")
    @ResponseWrapper(localName = "notesOnUsingTheTestHarnessResponse", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.NotesOnUsingTheTestHarnessResponse")
    public void notesOnUsingTheTestHarness(
        @WebParam(name = "notesOnUsingTheTestHarnessResult", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<java.lang.Object> notesOnUsingTheTestHarnessResult,
        @WebParam(name = "notes", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> notes);

    /**
     * 
     * @param password
     * @param eaVerificationWebServicesResult
     * @param message
     * @param userID
     */
    @WebMethod(action = "urn:JadeWebServices/WSP_Registry2/eaVerificationWebServices")
    @RequestWrapper(localName = "eaVerificationWebServices", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.EaVerificationWebServices")
    @ResponseWrapper(localName = "eaVerificationWebServicesResponse", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.EaVerificationWebServicesResponse")
    public void eaVerificationWebServices(
        @WebParam(name = "userID", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String userID,
        @WebParam(name = "password", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String password,
        @WebParam(name = "eaVerificationWebServicesResult", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<WSEaVerificationWebServicetResponse> eaVerificationWebServicesResult,
        @WebParam(name = "_message", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> message);

    /**
     * 
     * @param resumptionPoint
     * @param password
     * @param dateOfChange
     * @param userName
     * @param message
     * @param icpNotificationsV1Result
     */
    @WebMethod(operationName = "icpNotifications_v1", action = "urn:JadeWebServices/WSP_Registry2/icpNotifications_v1")
    @RequestWrapper(localName = "icpNotifications_v1", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpNotificationsV1")
    @ResponseWrapper(localName = "icpNotifications_v1Response", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpNotificationsV1Response")
    public void icpNotificationsV1(
        @WebParam(name = "userName", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String userName,
        @WebParam(name = "password", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String password,
        @WebParam(name = "dateOfChange", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        XMLGregorianCalendar dateOfChange,
        @WebParam(name = "resumptionPoint", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        int resumptionPoint,
        @WebParam(name = "icpNotifications_v1Result", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<WSICPNotificationResponse> icpNotificationsV1Result,
        @WebParam(name = "message", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> message);

    /**
     * 
     * @param password
     * @param icpId
     * @param icpDetailsV1Result
     * @param userName
     * @param message
     * @param eventDate
     */
    @WebMethod(operationName = "icpDetails_v1", action = "urn:JadeWebServices/WSP_Registry2/icpDetails_v1")
    @RequestWrapper(localName = "icpDetails_v1", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpDetailsV1")
    @ResponseWrapper(localName = "icpDetails_v1Response", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpDetailsV1Response")
    public void icpDetailsV1(
        @WebParam(name = "userName", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String userName,
        @WebParam(name = "password", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String password,
        @WebParam(name = "icpId", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String icpId,
        @WebParam(name = "eventDate", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        XMLGregorianCalendar eventDate,
        @WebParam(name = "icpDetails_v1Result", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<WSICPDetailsResponse> icpDetailsV1Result,
        @WebParam(name = "message", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> message);

    /**
     * 
     * @param icpId
     * @param includeMeter
     * @param includeNetwork
     * @param includeAddress
     * @param includeTrader
     * @param userName
     * @param message
     * @param includeSwitch
     * @param icpEventsV1Result
     * @param password
     * @param includePricing
     * @param includeStatus
     * @param includeReversed
     */
    @WebMethod(operationName = "icpEvents_v1", action = "urn:JadeWebServices/WSP_Registry2/icpEvents_v1")
    @RequestWrapper(localName = "icpEvents_v1", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpEventsV1")
    @ResponseWrapper(localName = "icpEvents_v1Response", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpEventsV1Response")
    public void icpEventsV1(
        @WebParam(name = "userName", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String userName,
        @WebParam(name = "password", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String password,
        @WebParam(name = "icpId", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String icpId,
        @WebParam(name = "includeReversed", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeReversed,
        @WebParam(name = "includeSwitch", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeSwitch,
        @WebParam(name = "includeTrader", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeTrader,
        @WebParam(name = "includeNetwork", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeNetwork,
        @WebParam(name = "includePricing", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includePricing,
        @WebParam(name = "includeStatus", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeStatus,
        @WebParam(name = "includeAddress", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeAddress,
        @WebParam(name = "includeMeter", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeMeter,
        @WebParam(name = "icpEvents_v1Result", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<WSICPEventsResponse> icpEventsV1Result,
        @WebParam(name = "message", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> message);

    /**
     * 
     * @param icpId
     * @param includeMeter
     * @param icpEventsV2Result
     * @param includeNetwork
     * @param includeAddress
     * @param includeTrader
     * @param userName
     * @param message
     * @param includeSwitch
     * @param password
     * @param includePricing
     * @param includeStatus
     * @param includeReversed
     */
    @WebMethod(operationName = "icpEvents_v2", action = "urn:JadeWebServices/WSP_Registry2/icpEvents_v2")
    @RequestWrapper(localName = "icpEvents_v2", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpEventsV2")
    @ResponseWrapper(localName = "icpEvents_v2Response", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpEventsV2Response")
    public void icpEventsV2(
        @WebParam(name = "userName", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String userName,
        @WebParam(name = "password", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String password,
        @WebParam(name = "icpId", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String icpId,
        @WebParam(name = "includeReversed", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeReversed,
        @WebParam(name = "includeSwitch", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeSwitch,
        @WebParam(name = "includeTrader", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeTrader,
        @WebParam(name = "includeNetwork", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeNetwork,
        @WebParam(name = "includePricing", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includePricing,
        @WebParam(name = "includeStatus", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeStatus,
        @WebParam(name = "includeAddress", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeAddress,
        @WebParam(name = "includeMeter", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean includeMeter,
        @WebParam(name = "icpEvents_v2Result", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<WSICPEventsResponseV2> icpEventsV2Result,
        @WebParam(name = "message", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> message);

    /**
     * 
     * @param password
     * @param meterComponentSerial
     * @param meterComponentType
     * @param userName
     * @param icpMeterIdSearchV1Result
     * @param message
     */
    @WebMethod(operationName = "icpMeterIdSearch_v1", action = "urn:JadeWebServices/WSP_Registry2/icpMeterIdSearch_v1")
    @RequestWrapper(localName = "icpMeterIdSearch_v1", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpMeterIdSearchV1")
    @ResponseWrapper(localName = "icpMeterIdSearch_v1Response", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpMeterIdSearchV1Response")
    public void icpMeterIdSearchV1(
        @WebParam(name = "userName", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String userName,
        @WebParam(name = "password", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String password,
        @WebParam(name = "meterComponentSerial", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String meterComponentSerial,
        @WebParam(name = "meterComponentType", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String meterComponentType,
        @WebParam(name = "icpMeterIdSearch_v1Result", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<WSICPSearchResponse> icpMeterIdSearchV1Result,
        @WebParam(name = "message", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> message);

    /**
     * 
     * @param streetOrPropertyName
     * @param streetOrPropertyFilter
     * @param isExactMatch
     * @param password
     * @param ownIcpsOnly
     * @param suburbOrTown
     * @param icpSearchV1Result
     * @param unitOrNumber
     * @param commissionedOnly
     * @param userName
     * @param region
     * @param message
     */
    @WebMethod(operationName = "icpSearch_v1", action = "urn:JadeWebServices/WSP_Registry2/icpSearch_v1")
    @RequestWrapper(localName = "icpSearch_v1", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpSearchV1")
    @ResponseWrapper(localName = "icpSearch_v1Response", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", className = "co.nz.electricityregistry.jadehttp.registryws.IcpSearchV1Response")
    public void icpSearchV1(
        @WebParam(name = "userName", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String userName,
        @WebParam(name = "password", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String password,
        @WebParam(name = "unitOrNumber", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String unitOrNumber,
        @WebParam(name = "streetOrPropertyName", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String streetOrPropertyName,
        @WebParam(name = "streetOrPropertyFilter", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String streetOrPropertyFilter,
        @WebParam(name = "suburbOrTown", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String suburbOrTown,
        @WebParam(name = "region", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        String region,
        @WebParam(name = "isExactMatch", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean isExactMatch,
        @WebParam(name = "ownIcpsOnly", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean ownIcpsOnly,
        @WebParam(name = "commissionedOnly", targetNamespace = "urn:JadeWebServices/WSP_Registry2/")
        boolean commissionedOnly,
        @WebParam(name = "icpSearch_v1Result", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<WSICPSearchResponse> icpSearchV1Result,
        @WebParam(name = "message", targetNamespace = "urn:JadeWebServices/WSP_Registry2/", mode = WebParam.Mode.OUT)
        Holder<String> message);

}
