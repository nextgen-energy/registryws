# RegistryWS [![pipeline status](https://gitlab.com/nextgen-energy/registryws/badges/master/build.svg)](https://gitlab.com/nextgen-energy/registryws/pipelines) ![code coverage](https://gitlab.com/nextgen-energy/registryws/badges/master/coverage.svg)

WSDL-Java-Binding for the Registry Web Services.

For use with Java/Kotlin, add the following to your `build.gradle`:

```gradle
repositories {
  maven { url "https://maven.nextgenenergy.co.nz/artifactory/maven/" }
}
dependencies {
  compile "co.nz.electricityregistry.jadehttp:wsregistry:20180910"
}
```
